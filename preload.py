#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

from processors.gcs_master import GCSMaster
from helpers.settings import settings
from fastapi.logger import logger

from pathlib import Path

# Download the Mask-RCNN model if it doesn't exist
local_file = "models/mask_rcnn_receipt_0030.h5"
if not Path(local_file).is_file():
    logger.warning("Model not found. Downloading...")
    GCSMaster(
        bucket_name=settings.models_bucket,
        cloud_blob=settings.mrcnn.cloud_blob,
        local_file=local_file,
        direction="down",
    )
    logger.info(f"Model downloaded to {local_file}")
