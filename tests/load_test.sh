#!/usr/bin/env bash

#host=34.91.155.97

for i in {1..10}
do
    curl --location --request POST '34.91.155.97/api/v1/vision/detectron/receipt' \
    --header 'X-API-Token: c7a9c910e0281a173c462063ec2f19b86dd26624f5516e29896dbd67c85c8219' \
    --header 'Content-Type: application/json' \
    --data @payload_detectron.json

    sleep 1

    curl --location --request POST '34.91.155.97/api/v1/vision/functions/binarization' \
    --header 'X-API-TOKEN: c7a9c910e0281a173c462063ec2f19b86dd26624f5516e29896dbd67c85c8219' \
    --header 'Content-Type: application/json' \
    --data @payload_binarization.json

    sleep 1

    curl --location --request POST '34.91.155.97/api/v1/vision/ocr/receipt' \
    --header 'X-API-Token: c7a9c910e0281a173c462063ec2f19b86dd26624f5516e29896dbd67c85c8219' \
    --header 'Content-Type: application/json' \
    --data @payload_ocr.json

    sleep 1

done
