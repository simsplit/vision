#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

import argparse


class ArgumentSwitcher(object):
    """Switches between various argument parsers for each of the implemented
     algorithms. It also inherits the module's globals for better OOP.
    """

    def select_module(self, module, doc):
        # Method name
        method_name = str(module) + "_args"
        # Get the method from 'self'. Default to a lambda.
        method = getattr(self, method_name, lambda: "Invalid module")
        # Call the method as we return it
        return method(doc)

    @staticmethod
    def blur_detection_args(doc):
        """Blur detection argument parser

        Example usage:
        $ python src/blur_detection.py \
                --input_path "/path/to/folder" \
                --filename "example_image.jpg" \
                --threshold 150 \
                --show

        Returns:
            _parser

        """
        _parser = argparse.ArgumentParser(description=doc)
        _parser.add_argument(
            "-i", "--input_path", type=str, help="folder containing image file"
        )
        _parser.add_argument(
            "-f", "--filename", type=str, help="name of image file"
        )
        _parser.add_argument(
            "-t",
            "--threshold",
            default=150,
            type=int,
            help="discrimination threshold (varies with dataset)",
        )
        _parser.add_argument(
            "--show",
            dest="show",
            action="store_true",
            help="show resulting image plots",
        )
        _parser.set_defaults(show=False)

        return _parser

    @staticmethod
    def image_stitcher_args(doc):
        """Image stitcher argument parser

        Example usage:
        $ python src/image_stitcher.py \
            --input_path "tmp/originals" \
            --output_path "tmp/stitched" \
            --filenames "1128-part-1.png" "1128-part-2.png" "1128-part-3.png" \
            --show

        Returns:
            _parser
        """
        _parser = argparse.ArgumentParser(description=doc)
        _parser.add_argument(
            "-i", "--input_path", type=str, help="folder containing image file"
        )
        _parser.add_argument(
            "-o", "--output_path", type=str, help="folder to output image file"
        )
        _parser.add_argument(
            "-f",
            "--filenames",
            nargs="+",
            type=str,
            help="name of image files (within same directory)",
        )
        _parser.add_argument(
            "--show",
            dest="show",
            action="store_true",
            help="show resulting image plots",
        )
        _parser.set_defaults(show=False)

        return _parser

    @staticmethod
    def su_binarization_args(doc):
        """SU Binarization argument parser

        Example usage:
        $ python src/su_binarization.py \
            --input_path "tmp/detections" \
            --output_path "tmp/su" \
            --filename "masked_image.png" \
            --show

        Returns:
            _parser
        """
        _parser = argparse.ArgumentParser(description=doc)
        _parser.add_argument(
            "-i", "--input_path", type=str, help="folder containing image file"
        )
        _parser.add_argument(
            "-o", "--output_path", type=str, help="folder to output image file"
        )
        _parser.add_argument(
            "-f", "--filename", type=str, help="name of image file"
        )
        _parser.add_argument(
            "--show",
            dest="show",
            action="store_true",
            help="show resulting image plots",
        )
        _parser.set_defaults(show=False)

        return _parser

    @staticmethod
    def line_segmentation_args(doc):
        """Line Segmentation argument parser

        Example usage:
        $ python src/line_segmentation.py \
            --input_path "tmp/su" \
            --segments_path "tmp/segments" \
            --boxes_path "tmp/boxes" \
            --filename "masked_image.png" \
            --show

        Returns:
            _parser
        """
        _parser = argparse.ArgumentParser(description=doc)
        _parser.add_argument(
            "-i", "--input_path", type=str, help="folder containing image file"
        )
        _parser.add_argument(
            "-s",
            "--segments_path",
            type=str,
            help="folder to output image segments",
        )
        _parser.add_argument(
            "-b",
            "--boxes_path",
            type=str,
            help="folder to output text-box detections",
        )
        _parser.add_argument(
            "-f", "--filename", type=str, help="name of image file"
        )
        _parser.add_argument(
            "--show",
            dest="show",
            action="store_true",
            help="show resulting image plots",
        )
        _parser.set_defaults(show=False)

        return _parser

    @staticmethod
    def ocr_args(doc):
        """OCR argument parser

        Example usage:
        $ python src/ocr.py \
            --filename "tmp/data.json"

        Returns:
            _parser
        """
        _parser = argparse.ArgumentParser(description=doc)
        _parser.add_argument(
            "-f", "--filename", type=str, help="name of .json mapping"
        )

        return _parser


if __name__ == "__main__":
    pass
