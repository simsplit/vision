#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

import re
from dateutil.parser import parse

# from spellchecker import SpellChecker

# Set column names for dataframe
column_definitions = {
    "segment_idx": "int",
    "segment_path": "str",
    "coords": "str",
    "centroid": "str",
    "row_pairs": "str",
    "col_pairs": "str",
    "detection_id": "int",
    "raw_text": "str",
    "text": "str",
    "wordshape": "str",
    # "is_tip",
    "is_percentage": "bool",
    "is_alpha": "bool",
    "is_numeric": "bool",
    "is_alphanumeric": "bool",
    "has_currency": "bool",
    "is_date": "bool",
    "is_url": "bool",
    "total_token": "str",
    "subtotal_token": "str",
    "tax_token": "str",
    "total": "float",
    "subtotal": "float",
    "tax": "float",
    "url": "str"
    # "is_phone_number"
}

attribute_map = {
    "has_total": "total",
    "has_subtotal": "subtotal",
    "has_tax": "tax",
    "has_change": "change",
    "has_cash": "cash",
    # 'has_currency_symbol': 'currency_symbol'
}

world_currency_symbols = (
    "د.إ",
    "Af",
    "L",
    "Դ",
    "Kz",
    "$",
    "ƒ",
    "ман",
    "КМ",
    "৳",
    "лв",
    "ب.د",
    "₣",
    "Bs.",
    "R$",
    "",
    "P",
    "Br",
    "¥",
    "₡",
    "Kč",
    "kr",
    "د.ج",
    "£",
    "Nfk",
    "€",
    "ლ",
    "₵",
    "D",
    "Q",
    "Kn",
    "G",
    "Ft",
    "Rp",
    "₪",
    "₹",
    "ع.د",
    "﷼",
    "Kr",
    "د.ا",
    "Sh",
    "៛",
    "₩",
    "د.ك",
    "〒",
    "₭",
    "ل.ل",
    "Rs",
    "ل.د",
    "د.م.",
    "ден",
    "K",
    "₮",
    "UM",
    "₨",
    "ރ.",
    "MK",
    "RM",
    "MTn",
    "₦",
    "C$",
    "ر.ع.",
    "B/.",
    "S/.",
    "₱",
    "zł",
    "₲",
    "ر.ق",
    "din",
    "р.",
    "ر.س",
    "Le",
    "Db",
    "ل.س",
    "฿",
    "ЅМ",
    "m",
    "د.ت",
    "T$",
    "₤",
    "₴",
    "Bs F",
    "₫",
    "Vt",
    "T",
    "R",
    "ZK",
)


# Main cleaning function
def clean_string(s, lang_code):
    # Keep only alphabet and _
    if lang_code == "en":
        s = re.sub("[^a-zA-Z0-9:.#$@/%]", " ", s)

    # Arabic unicode
    elif lang_code == "ar":
        s = re.sub(
            "[^0-9\u0621-\u064a\ufb50-\ufdff\ufe70-\ufefc_|+#$!*&@/~%-]",
            " ",
            s,
        )

    # French characters of accent, chapeau, etc..
    elif lang_code == "fr":
        s = re.sub("[^a-zA-ZÀ-ÿ0-9_|+#$!*&@/~%-]", " ", s)

    # German characters with umlauts
    elif lang_code == "de":
        s = re.sub("[^a-zA-Z0-9 ÄäÖöÜüß€_|+#$!*&@/~%-]", " ", s)

    # Swedish characters (vowels) ÅåÖöÄä
    elif lang_code == "sv":
        s = re.sub("[^a-zA-Z0-9 ÅåÖöÄä_|+#$!*&@/~%-]", " ", s)

    # Remove extra spaces
    s = re.sub(r"\s+", " ", s)

    # String spaces
    s = s.strip()

    return s


# Date detector
def is_date(string, fuzzy=False):
    """
    Return whether the string can be interpreted as a date.

    :param string: str, string to check for date
    :param fuzzy: bool, ignore unknown tokens in string if True
    """
    try:
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False

    except OverflowError:
        return False

    except TypeError:
        return False


# def check_spelling(words):
#     spell = SpellChecker()
#     list_of_words = words.split(' ')
#     # find those words that may be misspelled
#     misspelled = spell.unknown(list_of_words)
#
#     misspelled_words = []
#     for word in misspelled:
#         temp_list_of_words = [l.upper() for l in list_of_words]
#         word_idx = temp_list_of_words.index(word.upper())
#
#         # Get the one `most likely` answer
#         corrected = spell.correction(word)
#
#         misspelled_words.append((word_idx, word, corrected))
#
#     # print(misspelled_words)
#
#     for w in misspelled_words:
#         list_of_words[w[0]] = w[2]
#
#     return ' '.join(list_of_words)
