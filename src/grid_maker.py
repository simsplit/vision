# -*- coding: utf-8 -*-
import pprint

pp = pprint.PrettyPrinter(indent=4)

# centroid (x, y)
# box (x, y, w, h)
BOXES_CENTROIDS = [
    {"centroid": (50, 100), "box": (25, 75, 50, 125)},
    {"centroid": (275, 500), "box": (250, 475, 50,)},
    {"centroid": (1000, 300), "box": ()},
    {"centroid": (200, 100), "box": ()},
    {"centroid": (200, 300), "box": ()},
    {"centroid": (220, 400), "box": ()},
    {"centroid": (500, 30), "box": ()},
]

# BOXES_CENTROIDS = [
#     (5, 100),
#     (280, 500),
#     (1000, 300),
#     (200, 100),
#     (200, 300),
#     (220, 400),
#     (500, 30)
# ]

segment_dict = {
    "rc_sort": [
        {
            "coords": (62, 32, 224, 14),
            "centroid": (174, 39),
            "filename": "./tmp_ycj9uzc/segment_no_8.png",
        },
        {
            "coords": (103, 68, 346, 17),
            "centroid": (276, 76),
            "filename": "./tmp_ycj9uzc/segment_no_10.png",
        },
        {
            "coords": (112, 86, 327, 20),
            "centroid": (275, 96),
            "filename": "./tmp_ycj9uzc/segment_no_11.png",
        },
        {
            "coords": (120, 107, 307, 22),
            "centroid": (273, 118),
            "filename": "./tmp_ycj9uzc/segment_no_12.png",
        },
        {
            "coords": (0, 114, 37, 95),
            "centroid": (18, 161),
            "filename": "./tmp_ycj9uzc/segment_no_0.png",
        },
        {
            "coords": (234, 159, 80, 22),
            "centroid": (274, 170),
            "filename": "./tmp_ycj9uzc/segment_no_21.png",
        },
        {
            "coords": (198, 186, 146, 23),
            "centroid": (271, 197),
            "filename": "./tmp_ycj9uzc/segment_no_19.png",
        },
        {
            "coords": (410, 123, 103, 216),
            "centroid": (461, 231),
            "filename": "./tmp_ycj9uzc/segment_no_27.png",
        },
        {
            "coords": (32, 272, 175, 26),
            "centroid": (119, 285),
            "filename": "./tmp_ycj9uzc/segment_no_5.png",
        },
        {
            "coords": (32, 306, 233, 30),
            "centroid": (148, 321),
            "filename": "./tmp_ycj9uzc/segment_no_4.png",
        },
        {
            "coords": (32, 341, 231, 27),
            "centroid": (147, 354),
            "filename": "./tmp_ycj9uzc/segment_no_3.png",
        },
        {
            "coords": (415, 344, 94, 28),
            "centroid": (462, 358),
            "filename": "./tmp_ycj9uzc/segment_no_29.png",
        },
        {
            "coords": (33, 375, 158, 24),
            "centroid": (112, 387),
            "filename": "./tmp_ycj9uzc/segment_no_6.png",
        },
        {
            "coords": (412, 378, 96, 28),
            "centroid": (460, 392),
            "filename": "./tmp_ycj9uzc/segment_no_28.png",
        },
        {
            "coords": (35, 407, 324, 28),
            "centroid": (197, 421),
            "filename": "./tmp_ycj9uzc/segment_no_7.png",
        },
        {
            "coords": (404, 410, 103, 28),
            "centroid": (455, 424),
            "filename": "./tmp_ycj9uzc/segment_no_26.png",
        },
        {
            "coords": (5, 496, 30, 27),
            "centroid": (20, 509),
            "filename": "./tmp_ycj9uzc/segment_no_2.png",
        },
        {
            "coords": (242, 507, 260, 27),
            "centroid": (372, 520),
            "filename": "./tmp_ycj9uzc/segment_no_22.png",
        },
        {
            "coords": (342, 540, 158, 26),
            "centroid": (421, 553),
            "filename": "./tmp_ycj9uzc/segment_no_24.png",
        },
        {
            "coords": (94, 575, 34, 5),
            "centroid": (111, 577),
            "filename": "./tmp_ycj9uzc/segment_no_9.png",
        },
        {
            "coords": (143, 576, 62, 5),
            "centroid": (174, 578),
            "filename": "./tmp_ycj9uzc/segment_no_14.png",
        },
        {
            "coords": (211, 577, 128, 6),
            "centroid": (275, 580),
            "filename": "./tmp_ycj9uzc/segment_no_20.png",
        },
        {
            "coords": (359, 579, 43, 5),
            "centroid": (380, 581),
            "filename": "./tmp_ycj9uzc/segment_no_25.png",
        },
        {
            "coords": (285, 597, 213, 26),
            "centroid": (391, 610),
            "filename": "./tmp_ycj9uzc/segment_no_23.png",
        },
        {
            "coords": (4, 526, 33, 193),
            "centroid": (20, 622),
            "filename": "./tmp_ycj9uzc/segment_no_1.png",
        },
        {
            "coords": (140, 691, 254, 28),
            "centroid": (267, 705),
            "filename": "./tmp_ycj9uzc/segment_no_13.png",
        },
        {
            "coords": (184, 724, 165, 28),
            "centroid": (266, 738),
            "filename": "./tmp_ycj9uzc/segment_no_18.png",
        },
        {
            "coords": (181, 757, 168, 28),
            "centroid": (265, 771),
            "filename": "./tmp_ycj9uzc/segment_no_16.png",
        },
        {
            "coords": (184, 791, 161, 29),
            "centroid": (264, 805),
            "filename": "./tmp_ycj9uzc/segment_no_17.png",
        },
        {
            "coords": (181, 896, 160, 27),
            "centroid": (261, 909),
            "filename": "./tmp_ycj9uzc/segment_no_15.png",
        },
    ]
}


class GridMaker:
    """The GridMaker

    """

    def __init__(self, segment_dict):
        self.segment_dict = segment_dict

    @property
    def grid(self):
        """Creates a grid based on the centroids

        Returns:
            grid (:obj:`dict`)

        """
        # return self.segment_dict
        __grid = {
            "c_sort": sorted(
                self.segment_dict, key=lambda x: x["centroid"][0]
            ),
            "r_sort": sorted(
                self.segment_dict, key=lambda x: x["centroid"][1]
            ),
            "cr_sort": sorted(
                self.segment_dict,
                key=lambda x: (x["centroid"][0], x["centroid"][1]),
            ),
            "rc_sort": sorted(
                self.segment_dict,
                key=lambda x: (x["centroid"][1], x["centroid"][0]),
            ),
        }
        return __grid

    @property
    def row_collation(self):
        """

        Returns:

        """
        grid_len = len(self.grid["rc_sort"])

        # keep track of i index
        i_list = []

        # Store rows
        rows = {}
        enum_idx = 0
        for i in range(grid_len):
            # Make sure we dont double loop
            if i not in i_list:
                # add current index to finished indexes
                i_list.append(i)

                # Store current row
                row = []

                # Get current box
                box1 = self.grid["rc_sort"][i]

                # Obtain centroid values
                c1 = box1["centroid"]
                x, y = c1[0], c1[1]

                # Append box to the current row
                row.append(box1)

                # Loop over the segments again
                for j in range(grid_len):

                    # Do not repeat i
                    if j not in i_list:

                        # Get current box
                        box2 = self.grid["rc_sort"][j]

                        co = box2["coords"]

                        x1, y1 = co[0], co[1]
                        x2, y2 = co[2] + co[0], co[3] + co[1]

                        # if box2 is a row pair of box1
                        if y1 <= y <= y2:
                            # Add the box to the row
                            row.append(box2)

                            # Add to list of looped over indexes
                            i_list.append(j)
                # print(f"ROW: {row}")
                # Append the current row to the rows
                rows[enum_idx] = row
                enum_idx += 1
        return rows

    @property
    def column_collation(self):
        """

        Returns:

        """
        grid_len = len(self.grid["cr_sort"])

        # keep track of i index
        i_list = []

        # Store rows
        cols = {}
        enum_idx = 0
        for i in range(grid_len):
            # Make sure we dont double loop
            if i not in i_list:
                # add current index to finished indexes
                i_list.append(i)

                # Store current row
                col = []

                # Get current box
                box1 = self.grid["cr_sort"][i]

                # Obtain centroid values
                c1 = box1["centroid"]
                x, y = c1[0], c1[1]

                # Append box to the current row
                col.append(box1)

                # Loop over the segments again
                for j in range(grid_len):

                    # Do not repeat i
                    if j not in i_list:

                        # Get current box
                        box2 = self.grid["cr_sort"][j]

                        co = box2["coords"]

                        x1, y1 = co[0], co[1]
                        x2, y2 = co[2] + co[0], co[3] + co[1]

                        # if box2 is a row pair of box1
                        if x1 <= x <= x2:
                            # Add the box to the row
                            col.append(box2)

                            # Add to list of looped over indexes
                            i_list.append(j)
                # print(f"ROW: {row}")
                # Append the current row to the rows
                cols[enum_idx] = col
                enum_idx += 1
        return cols


if __name__ == "__main__":
    gm = GridMaker(segment_dict=segment_dict)
    gm.collated_grid
