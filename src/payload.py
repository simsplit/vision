"""
@File name:      payload.py
@Type:           Data Class
@Directory:      src
@Repo name:      vision
@Author:         Yanal Kashou
@Organization:   SimSplit
@Email:          yanal.kashou@simsplit.co
"""
#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

from dataclasses import dataclass


# ------------------------------------------------------
# Payload Dataclass
# ------------------------------------------------------
@dataclass
class Payload:
    """
    Creates a JSON payload to be returned upon request

    The goal of this is to standardize the payload.
    """

    # Status Code
    code: int = None

    # Message
    msg: str = None

    # Body
    body: dict = None

    def __post_init__(self):

        if self.code is None:
            self.code = 200

        if self.msg is None:
            self.msg = "success"

        self.meta = {
            "response": {
                "message": self.msg,
                "status_code": self.code,
                "body": self.body,
            }
        }
