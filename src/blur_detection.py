#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

__doc__ = """Blur Detector is an algorithm that classifies images as blurry or
not by thresholding the variance of the Laplacian of the image."""

import cv2
import numpy as np
from dataclasses import dataclass


@dataclass
class BlurDetector:
    """Measures the variance of the Laplacian of the image.

    For receipt images in the wild, this value is discriminatory
    at threshold=150.
    
    Args:
        filename (:obj:`str`): The image file to process.
        show (:obj:`bool`): True to plot the results, False to skip. \
        Defaults to False.
        threshold (:obj:`int`): An integer threshold that determines if \
        the image is blurry or not. Calibrated to a default of 150 for \
        real-life images of receipts.

    Examples:
        >>> BlurDetector(filename="path/to/image.png")

    """

    # Required Fields
    filename: str

    # Optional Fields
    show: bool = None
    threshold: int = 150

    def __post_init__(self):

        image = cv2.imread(self.filename)

        # Converts image to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Calculates the variance
        varlap = self.variance_of_laplacian(gray)

        # Sets the default as not blurry
        self.is_blurry = False
        text = "Not Blurry"

        # If the variance is less than the threshold
        if varlap < self.threshold:
            # Flag as blurry
            self.is_blurry = True
            text = "Blurry"

        # If images are to be shown
        if self.show:
            # Put the computed variance on the image
            cv2.putText(
                image,
                "{}: {:.2f}".format(text, varlap),
                (10, 30),
                cv2.FONT_HERSHEY_DUPLEX,
                0.8,
                (255, 255, 255),
                1,
            )
            # Show the image
            cv2.imshow("Image", image)
            cv2.waitKey(0)

    # from image_paths import IMAGE_PATHS
    @staticmethod
    def variance_of_laplacian(img):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        return cv2.Laplacian(img, cv2.CV_64F).var()


# if __name__ == '__main__':
#     # Import the ArgumentSwitcher
#     from argument_switcher import ArgumentSwitcher
#     # Initialize an instance
#     args_switcher = ArgumentSwitcher()
#     # Select parser module and pass global __doc__
#     parser = args_switcher.select_module(
#         module='blur_detection',
#         doc=globals()['__doc__']
#     )
#     # Initialize parser arguments
#     args = parser.parse_args()
#     # Pass converted namespace as dictionary of arguments
#     bd = BlurDetector(**vars(args))
#     print(f"[is_blurry]: {bd.is_blurry}")
