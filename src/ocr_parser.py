# -*- coding: utf-8 -*-
"""`ocr_parser.py` contains an implementation of the :class:`OCRParse`
class.
"""
from src.ocr_helpers import clean_string, is_date
from processors.parser_regex import (
    PRICE_REGEX,
    DIGIT_REGEX,
    PERCENTAGE_REGEX,
    URL_REGEX,
)
from fastapi.logger import logger
from fuzzywuzzy import fuzz, process
import numpy as np
import re


class OCRParse:
    """A class dedicated to parsing the text extracted by the OCR.

    Parsing capabilities:

        *   Total
        *   Subtotal
        *   Tax
        *   Cash
        *   Change

    """

    def __init__(self, text_list, token_attribute):
        self.__text_list = text_list
        self.__token_attribute = token_attribute
        # self.data = getattr(self, attribute)

    @staticmethod
    def extract_amount(text_list):
        """Unified static method that simply extracts amounts from text
        lists.

        Args:
            text_list ():

        Returns:

        """
        __amount = []
        for text in text_list:
            text = text.replace(" ", "")
            extracted = re.findall(DIGIT_REGEX, text.replace(",", "."))
            __amount.extend(extracted)

        joined = "".join(__amount)

        if len(__amount) > 2:
            return None
        elif len(__amount) <= 2:
            return joined

    @property
    def data(self):
        # logger.info(f"Token Attribute: {self.__token_attribute}")
        return getattr(self, self.__token_attribute)

    @property
    def text_list(self):
        """Create a property out of the list of extracted text by the
        OCR.
        """
        return self.__text_list

    @property
    def url(self):
        """Check if text is a url"""
        t = self.text_list
        if self.is_url:
            # logger.info(t)
            url = re.findall(URL_REGEX, t)
            # logger.info(url)
            return url
        else:
            return None

    @property
    def total(self):
        """

        Returns:

        """
        text_list = self.text_list

        return self.extract_amount(text_list)

    @property
    def subtotal(self):
        """

        Returns:

        """
        text_list = self.text_list

        return self.extract_amount(text_list)

    @property
    def tax(self):
        """

        Returns:

        """
        text_list = self.text_list

        return self.extract_amount(text_list)

    @property
    def cash(self):
        """

        Returns:

        """
        text_list = self.text_list

        return self.extract_amount(text_list)

    @property
    def change(self):
        """

        Returns:

        """
        text_list = self.text_list

        return self.extract_amount(text_list)
