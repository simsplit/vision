#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

import cv2
import numpy as np


def detectAndDescribe(image, method=None):
    """
    Compute key points and feature descriptors using an specific method

    SIFT and SURF are propietary algorithms
    BRISK and ORB are open-source algorithms

    They all work. SIFT AND SURF require CMAKE compilation of OpenCV.
    """

    assert (
        method is not None
    ), "You need to define a feature \
    detection method. Values are: 'sift', 'surf'"

    # detect and extract features from the image
    if method == "sift":
        descriptor = cv2.xfeatures2d.SIFT_create()
    elif method == "surf":
        descriptor = cv2.xfeatures2d.SURF_create()
    elif method == "brisk":
        descriptor = cv2.BRISK_create()
    elif method == "orb":
        descriptor = cv2.ORB_create()

    # get keypoints and descriptors
    (kps, features) = descriptor.detectAndCompute(image, None)

    return kps, features


def createMatcher(method, crossCheck):
    """Create and return a Matcher Object"""

    if method == "sift" or method == "surf":
        bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=crossCheck)
    elif method == "orb" or method == "brisk":
        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=crossCheck)
    return bf


def matchKeyPointsBF(featuresA, featuresB, method):
    """Bruteforce keypoint matcher"""
    bf = createMatcher(method, crossCheck=True)

    # Match descriptors.
    best_matches = bf.match(featuresA, featuresB)

    # Sort the features in order of distance.
    # The points with small distance (more similarity) are ordered first
    # in the vector
    raw_matches = sorted(best_matches, key=lambda x: x.distance)
    print("Raw matches (Brute force):", len(raw_matches))
    return raw_matches


def matchKeyPointsKNN(featuresA, featuresB, ratio, method):
    """K-Nearest Neighbours keypoint matcher"""
    bf = createMatcher(method, crossCheck=False)

    # compute the raw matches and initialize the list of actual matches
    raw_matches = bf.knnMatch(featuresA, featuresB, 2)
    print("Raw matches (knn):", len(raw_matches))

    matches = []
    # loop over the raw matches
    for m, n in raw_matches:
        # ensure the distance is within a certain ratio of each
        # other (i.e. Lowe's ratio test)
        if m.distance < n.distance * ratio:
            matches.append(m)
    return matches


def getHomography(kps_a, kps_b, matches, reproject_threshold):
    """Calculate Homography between 2 sets of keypoints"""
    # convert the keypoints to numpy arrays
    kps_a = np.float32([kp.pt for kp in kps_a])
    kps_b = np.float32([kp.pt for kp in kps_b])

    if len(matches) > 4:

        # construct the two sets of points
        pts_a = np.float32([kps_a[m.queryIdx] for m in matches])
        pts_b = np.float32([kps_b[m.trainIdx] for m in matches])

        # estimate the homography between the sets of points
        (H, status) = cv2.findHomography(
            pts_a, pts_b, cv2.RANSAC, reproject_threshold
        )

        return matches, H, status
    else:
        return None
