# -*- coding: utf-8 -*-
from src.ocr_helpers import clean_string, is_date
from processors.parser_regex import (
    PRICE_REGEX,
    DIGIT_REGEX,
    PERCENTAGE_REGEX,
    URL_REGEX,
)
from fastapi.logger import logger
from fuzzywuzzy import fuzz, process
import numpy as np
import re


class OCRSense:
    """OCRSense is a parser for the JSON file produced by
    :class:`LineSegmenter`, specifically the text extracted from a segment
    of the JSON file.. It allows us to seamlessly tackle each key of
    interest and finally arrange thing into a :class:`pd.DataFrame` object.

    This is particularly crucial because a tabular view is much more
    expressive in this case and will allow us to filter detections to
    contruct an appropriate response as well as provide us with an
    analytical perspective, where each column is a feature.

    """

    def __init__(self, text):
        """Constructor

        Args:
            text (:obj:`str`) The key currently being looped over.
            v: The value currently being looper over.
        """
        self.__text = text

    # GENERAL PROPERTIES
    @property
    def raw_text(self):
        """Return the raw text"""
        t = self.__text
        return t

    @property
    def text(self):
        """Clean the text based on language"""
        t = self.__text
        return clean_string(t, "en")

    @property
    def wordshape(self):
        """

        Returns:

        """
        t = self.text

        t1 = re.sub("[A-Z]", "X", t)
        t2 = re.sub("[a-z]", "x", t1)
        t3 = re.sub("[0-9]", "d", t2)
        t4 = re.sub("[$€]", "p", t3)
        t5 = re.sub("[~!@#%^&*()_+=-]", "s", t4)

        return t5

    @property
    def wordlen(self):
        """

        Returns:

        """
        t = self.text

        return len(t)

    @property
    def booleans(self):
        """

        Returns:

        """
        t = self.text

        # Alpha, numeric and alphanumeric
        is_alpha = t.replace(" ", "").isalpha()
        is_numeric = t.replace("$", "").isnumeric()
        is_alphanumeric = t.replace(" ", "").isalnum()

        # Percentage
        is_percentage = "%" in self.wordshape

        # Time and Date
        is_time = False
        # is_date = is_date()
        is_date = False

        # Address and Phone Number
        is_address = False
        is_phone_number = False

        # URL
        url_rules = [
            t.startswith("http"),
            t.startswith("https"),
            ".com" in t,
            ".org" in t,
            ".net" in t,
            ".io" in t,
            "www" in t,
        ]
        is_url = any(url_rules)

        # Item
        is_item = False

        # Quantity
        is_quantity = False

        return dict(
            is_alpha=is_alpha,
            is_numeric=is_numeric,
            is_alphanumeric=is_alphanumeric,
            is_percentage=is_percentage,
            is_time=is_time,
            is_date=is_date,
            is_address=is_address,
            is_phone_number=is_phone_number,
            is_url=is_url,
            is_item=is_item,
            is_quantity=is_quantity,
        )

    @property
    def tokens(self):
        """

        Returns:

        """
        t = self.text

        # Currency
        has_currency_symbol = any(["$" in t, "€" in t, "£" in t])

        # Tip
        has_tip = fuzz.token_sort_ratio("tip", t) >= 85

        # Change
        has_change = fuzz.token_sort_ratio("change", t) >= 85

        # Cash
        has_cash = fuzz.token_sort_ratio("cash", t) >= 85

        # Tax
        has_tax = fuzz.token_sort_ratio("tax", t) >= 85

        # Ratios: Subtotal and Total
        ratios = {
            "subtotal": fuzz.WRatio("subtotal", t),
            "total": fuzz.WRatio("total", t),
        }

        # Subtotal
        has_subtotal = all(
            [ratios["subtotal"] >= 85, ratios["subtotal"] >= ratios["total"]]
        )

        # Total
        has_total = all([ratios["total"] >= 85, has_subtotal is False])

        return dict(
            has_currency_symbol=has_currency_symbol,
            has_tip=has_tip,
            has_change=has_change,
            has_cash=has_cash,
            has_tax=has_tax,
            has_subtotal=has_subtotal,
            has_total=has_total,
        )

    @property
    def results(self):
        """Check if text is a phone number"""
        _results = dict(
            raw_text=self.raw_text,
            text=self.text,
            wordshape=self.wordshape,
            wordlen=self.wordlen,
            booleans=self.booleans,
            tokens=self.tokens,
        )
        return _results
