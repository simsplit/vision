#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

import numpy as np

nfns = [
    lambda x: np.roll(x, -1, axis=0),
    lambda x: np.roll(np.roll(x, 1, axis=1), -1, axis=0),
    lambda x: np.roll(x, 1, axis=1),
    lambda x: np.roll(np.roll(x, 1, axis=1), 1, axis=0),
    lambda x: np.roll(x, 1, axis=0),
    lambda x: np.roll(np.roll(x, -1, axis=1), 1, axis=0),
    lambda x: np.roll(x, -1, axis=1),
    lambda x: np.roll(np.roll(x, -1, axis=1), -1, axis=0),
]


def localminmax(img, fns):
    mi = img.astype(np.float64)
    ma = img.astype(np.float64)
    for i in range(len(fns)):
        rolled = fns[i](img)
        mi = np.minimum(mi, rolled)
        ma = np.maximum(ma, rolled)
    result = (ma - mi) / (mi + ma + 1e-16)
    return result


def numnb(bi, fns):
    nb = bi.astype(np.float64)
    i = np.zeros(bi.shape, nb.dtype)
    i[bi == bi.max()] = 1
    i[bi == bi.min()] = 0
    for fn in fns:
        nb += fn(i)
    return nb


def rescale(r, maxvalue=255):
    mi = r.min()
    return maxvalue * (r - mi) / (r.max() - mi)
