# -*- coding: utf-8 -*-
"""ocr.py

Our main OCR is an implementation of Tesseract 4 and relies heavily on \
sane and sound pre-processing of the images. It requires not an image,
but a .json mapping of image files.

"""

#  ---------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  ---------------------------------------------------------------------


import json
import pprint
import cv2
import pytesseract
import pandas as pd
from fuzzywuzzy import fuzz
from fastapi.logger import logger
from dataclasses import dataclass

# OCR helper functions and variables
from src.ocr_helpers import attribute_map

# For logic and making sense of the OCR text
from src.ocr_sensor import OCRSense

# For actual parsing and extraciton of the OCR text
from src.ocr_parser import OCRParse

# For temporary directories
from tempfile import TemporaryDirectory

from collections import Counter

pp = pprint.PrettyPrinter(indent=2)


class OCR:
    """OCR, this is what parses everything remaining. Contains the logic
    to do spellchecks, regex, digit and alpha detection etc... So,
    essentially, runs the detections, and then attempts to fix or
    improve the detections as much as possible.

    Examples:
        >>> # If "data/detections.json" exists
        >>> OCR()
        >>> # To specify a file
        >>> OCR(filename="path/to/detections.json")

    """

    def __init__(self, temp_dir: TemporaryDirectory, filename: str = None):
        """
        Args:
            temp_dir (:obj:`TemporaryDirectory`): An object to \
            manipulate temporary directory created for processing.
            filename (:obj:`str`): A string containing the filename.

        Returns:
            results (:obj:`dict`): A results dictionary containing all \
            information to be returned by the OCR.
        """
        self.temp_dir = temp_dir
        self.filename = filename
        self.data = None
        self.__results = {}

    @property
    def results(self):
        """A property to access the OCR results to be returned in the
        request.

        Returns:
            results (:obj:`dict`): The extracted values.

        """
        return self.__results

    @staticmethod
    def run_tesseract(image) -> str:
        """Tesseract has multiple modes for detection optimization:

        Page segmentation modes:
            0. Orientation and script detection (OSD) only.
            1. Automatic page segmentation with OSD.
            2. Automatic page segmentation, but no OSD, or OCR.
            3. Fully automatic page segmentation, but no OSD. (Default)
            4. Assume a single column of text of variable sizes.
            5. Assume a single uniform block of vertically aligned text.
            6. Assume a single uniform block of text.
            7. Treat the image as a single text line.
            8. Treat the image as a single word.
            9. Treat the image as a single word in a circle.
            10. Treat the image as a single character.
            11. Sparse text. Find as much text as possible in no \
            particular order.
            12. Sparse text with OSD.
            13. Raw line. Treat the image as a single text line, \
            bypassing hacks that are Tesseract-specific.

        Args:
            image (:obj:`cv2.image`): A preprocessed image array.
        """
        # Set the mode
        configuration = "-l eng --oem 1 --psm 6"

        # Run the ocr
        text = pytesseract.image_to_string(image, config=configuration)

        # Return the detected text
        return text

    def compute(self) -> dict:
        """Main function


        Examples:

        """
        # Assign variable to TemporaryDirectory object
        temp_dir = self.temp_dir

        # Construct filename
        filename = f"{temp_dir.name}/{self.filename}"

        # Print the detections collected so far and save to file
        with open(filename) as f:
            detections = json.load(f)

        # Create an empty dicionary to hold results
        results_dict = {}

        # Create a tracker for the enumeration of the new dictionary
        results_dict_idx = 0

        # Attribute
        token_attribute = None

        # Wordshape consensus
        wordshape_consensus = None

        # Loop over the enumerated dictionary
        # k is the index
        # v is the list of detection dictionaries
        for k, v in detections.items():

            results = []

            row_text_list = []

            # For each dictionary item in the list
            for item in v:

                # Read the image
                image = cv2.imread(item["filename"])

                # Run the detection
                text = self.run_tesseract(image)

                # Strip and Split the text by new-lines (the detector is
                # set at mode 6 which understands "blocks of text", not
                # just "lines")
                text_list = text.strip().split("\n")

                # Flag if text is available
                text_available = False

                # Since a large box may be present, then what is
                # produced is a list of text detections (After splitting
                # by new lines. So we need to loop over the list.
                for t in text_list:

                    # If a detection is made (not an empty string)
                    if t:
                        row_text_list.append(t)
                        # Flag if text is available
                        text_available = True
                        # Run OCRParser
                        ocr_sense = OCRSense(t)
                        sense_results = ocr_sense.results

                        # Create Wordshape conditions to rule out single
                        # and double letter detections. (Does not remove
                        # digits, but does clean up artifacts.)
                        # Causes tremendous decrease is JSON output
                        # sometimes, which is better and cleaner.
                        wshape_rules = [
                            sense_results["wordshape"].lower() != "x",
                            sense_results["wordshape"].lower() != "xx",
                            sense_results["wordlen"] <= 35,
                        ]

                        # If all shape rules are met (negation), then
                        # unpack dictionaries and append to results, as
                        # a single dictionary 'final'
                        if all(wshape_rules):
                            final = {**item, **sense_results}
                            results.append(final)

                        # Get the token attribute that has been flagged
                        # as True. Token attributes are unique, so this
                        # works trivially.
                        for m, n in attribute_map.items():
                            if sense_results["tokens"][m]:
                                token_attribute = n

            # If the text_list has text available, and if a token
            # attribute is True.
            if token_attribute:
                # logger.info(f"Text List: {text_list}")
                # Text is already in a list, so the parser can run here,
                # with the flag required.
                # logger.info(f"Token: {token_attribute}")
                ocr_parse = OCRParse(
                    text_list=row_text_list, token_attribute=token_attribute
                )

                # # Get most common wordshape between processed items
                # wordshape_counter = Counter(wordshape_list).most_common(1)
                #
                # # The result is always a list containing a tuple like:
                # # [('dd.dd', 3)]
                # wordshape_consensus = wordshape_counter[0][0]

                # logger.info(ocr_parse.data)

                # TODO: Join results that have specific wordshapes
                #   [dd, dd]      ~ 10 00     ->  10.00
                #   [dd., dd]     ~ 10. 00    ->  10.00
                #   [dd, .dd]     ~ 10 .00    ->  10.00
                #   [ddd, dd]     ~ 100 00    ->  100.00
                #   [ddd., dd]    ~ 100. 00   ->  100.00
                #   [ddd, .dd]    ~ 100 .00   ->  100.00

                # TODO: Add sensing and parsing of text representing
                #   the total such as 'Amount Due', or 'Total Due'

                # TODO: Add column-based mitigation for lack of token
                #   detection, by finding the argmax of the list of
                #   extracted prices.

                # TODO: Drop keys that do not meet the wordshape:
                #   'dd.dd' or 'ddd.dd' (after corrective joining)

                # TODO: Add arithmetic sanity checker for list of
                #   values. It is a fair assumption to make that the
                #   either the highest or 2nd highest value extracted
                #   actually represents the total. (2nd highest in case
                #   there is a given cash amount like paying $200 for a
                #   $150 total. So there must be additive arithmetic
                #   sanity in either numbers.

                self.__results[token_attribute] = ocr_parse.data
                token_attribute = None

                del ocr_parse

            # If the results list is not empty
            if results:

                # Set the value of the current index of the results
                # dictionary to the current results list
                results_dict[results_dict_idx] = results

                # Increment the index
                results_dict_idx += 1

        # Output to JSON
        with open(filename, "w") as f:
            json.dump(results_dict, f)


if __name__ == "__main__":
    # Import the ArgumentSwitcher
    from argument_switcher import ArgumentSwitcher

    # Initialize an instance
    args_switcher = ArgumentSwitcher()
    # Select parser module and pass global __doc__
    parser = args_switcher.select_module(
        module="ocr", doc=globals()["__doc__"]
    )
    # Initialize parser arguments
    args = parser.parse_args()
    # Pass converted namespace as dictionary of arguments
    OCR(**vars(args))
