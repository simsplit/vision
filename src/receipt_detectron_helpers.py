#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

# ------------------------------------------------------
# Receipt Detectron (MRCNN) Helpers
# ------------------------------------------------------
import numpy as np
import cv2
import colorsys
import random
from scipy import stats


def random_colors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors


def compute_centroid_patch_mode(image):
    """Finds the centroid of the image, draws a square with edges equal
    to of 25% of the original width. Finds the mode.

    Note: When the mode is computed on the original image, it is
        problematic because some images post-cropping and pre-masking
        have dominantly dark backgrounds, this shifts the mode into a
        low value, causing great issues in the line-segmentation stage.
        The mode should  reflect the inner mask, rather than the outer.

    This is a trick to find the most common pixel value of the receipt.
    Receipts are centered and hence finding the centroid allows us to
    select the very middle of the receipt, extrapolating outwards 25%
    towards the edges. Then computing the inner mode to be used in the
    mask application.
    """

    h, w, _ = image.shape

    x = 0
    y = 0

    x_offset = int(w * 0.25)
    y_offset = int(h * 0.25)

    centroid = ((x + h) // 2, (y + w) // 2)

    roi = image[
        centroid[0] - y_offset : centroid[0] + y_offset,
        centroid[1] - x_offset : centroid[1] + x_offset,
    ]

    # Compute mode
    x = stats.mode(roi, axis=None)
    _mode = int(x.mode)

    return _mode


# This is where the magic happens
def apply_mask(image, image_mode, mask, color, alpha=0.5):
    """Apply the given mask to the image.

    Args:
        image ()
        image_mode ()
        mask ()
        color ()
        alpha ()


    Returns:
        image ()
    """
    for c in range(3):
        # Inner Mask
        # image[:, :, c] = np.where(mask == 1,
        #                           image[:, :, c] *
        #                           (1 - alpha) + alpha * color[c] * 255,
        #                           image[:, :, c])

        # External Mask (set to white)
        image[:, :, c] = np.where(
            mask == 0,
            image_mode,
            # 255,
            image[:, :, c],
        )
    return image
