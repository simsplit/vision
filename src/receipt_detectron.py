#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

# Dataclasses and Type hinting
from dataclasses import dataclass

# MRCNN Loader Class1
from helpers.settings import settings

import logging
import numpy as np

# Image saving and loading
from PIL import Image

from .receipt_detectron_helpers import (
    random_colors,
    apply_mask,
    compute_centroid_patch_mode,
)

# from .mrcnn_loader import graph

# Contouring and plotting
from skimage.measure import find_contours
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from uuid import uuid4
from tempfile import TemporaryDirectory
import cv2

# pprint
import pprint

pp = pprint.PrettyPrinter(indent=4)
logging.getLogger("mrcnn").setLevel(logging.ERROR)

mrcnn_model, graph = settings.mrcnn.model_loader()


def get_ax(rows=1, cols=1, size=16):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.

    Adjust the size attribute to control how big to render images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
    return ax


# ------------------------------------------------------
# ReceiptDetectron Dataclass
# ------------------------------------------------------
@dataclass
class ReceiptDetectron:
    """
    Input:
        - A list of image paths (either local or via GCP bucket)

    Pipeline:
        - Detect receipt mask using M-RCNN
        - Crop out the mask and place onto blank canvas
        - Align the text

    Output:
        - 'mrcnn_detection' attribute:
            Output of detection via mask-rcnn model

        - 'unmasked' attribute:
            Output of element-wise subtraction of mask from the image.

        - 'homography' attribute:
            Output of computed homography for perspective correction.

        - 'corrected_perspective' attribute:
            Output of perspective correction - an image binary as np.ndarray.

        - 'status' attribute:
            An indication of the current processing status.
            Helps in keeping track of the algorithms.
    """

    # Instantiates a list of image arrays
    # image_paths: List[str]
    image_path: str
    temp_dir: TemporaryDirectory

    def __post_init__(self):
        self.cropped_image_name = "cropped.png"
        self.masked_image_name = "masked.png"

        # Read image
        image = cv2.imread(self.image_path)

        # Run detectron
        with graph.as_default():
            detection = mrcnn_model.detect([image], verbose=1)[0]

        self.unmasked = self.unmask(image, detection, 0)
        self.draw_mask(image, detection)

    def unmask(self, img, det, k):
        """
        Define a simple CUDA kernel for element-wise subtraction

        This way the image masks (m) can be subtracted quickly
        from the original image (o) to return the result (z).
        """
        # matrix_subtraction = cp.ElementwiseKernel(
        #     'float32 o, float32 m',
        #     'float32 z',
        #     'z = o - m',
        #     'matrix_subtraction'
        # )
        # unmasked = matrix_subtraction(self.img, self.mask)

        rois = det["rois"][0]

        cropped_img = img[rois[0] : rois[2], rois[1] : rois[3]]

        # Image.fromarray(cropped_img).save(
        #     f"tmp/detections/cropped_image_{k}.png"
        # )
        Image.fromarray(cropped_img).save(
            f"{self.temp_dir.name}/{self.cropped_image_name}"
        )
        print(self.cropped_image_name)
        return 1
        # return unmasked

    # So, the polygons are the edges. The following draws them,
    # but that is not the point. The point is finding them,
    # and then
    def draw_mask(self, img, det):
        """

        Args:
            img ():
            det ():

        Returns:

        """
        # figsize = (16, 16)
        # _, ax = plt.subplots(1, figsize=figsize)
        #
        # # Generate random colors
        # # Show area outside image boundaries.
        #
        # height, width = img.shape[:2]
        # ax.set_ylim(height + 10, -10)
        # ax.set_xlim(-10, width + 10)
        # ax.axis('off')
        # ax.set_title("prediction")

        # Get image mode
        image_mode = compute_centroid_patch_mode(img)

        # Boxes
        N = det["rois"].shape[0]
        # N = 3
        colors = random_colors(N)
        masks = det["masks"]

        masked_image = img.astype(np.uint32).copy()
        for i in range(N):
            color = colors[i]
            # Mask
            mask = masks[:, :, i]
            # if show_mask:
            masked_image = apply_mask(masked_image, image_mode, mask, color)

            # Mask Polygon
            # Pad to ensure proper polygons for masks that touch image edges.
            padded_mask = np.zeros(
                (mask.shape[0] + 2, mask.shape[1] + 2), dtype=np.uint8
            )
            padded_mask[1:-1, 1:-1] = mask
            contours = find_contours(padded_mask, 0.5)

            while i < (N // 2):
                for verts in contours:
                    # Subtract the padding and flip (y, x) to (x, y)
                    verts = np.fliplr(verts) - 1
                    p = Polygon(verts, facecolor="none", edgecolor=color)
                    ax.add_patch(p)

        # Cropping
        img = masked_image.astype(np.uint8).copy()
        rois = det["rois"][0]
        masked_image = img[rois[0] : rois[2], rois[1] : rois[3]]

        Image.fromarray(masked_image).save(
            f"{self.temp_dir.name}/{self.masked_image_name}"
        )
        print(self.masked_image_name)
