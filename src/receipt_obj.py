"""
@File name:      receipt_obj.py
@Type:           Data Class
@Directory:      src/data_classes
@Repo name:      vision
@Author:         Yanal Kashou
@Organization:   SimSplit
@Email:          yanal.kashou@simsplit.co
"""
#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

from dataclasses import dataclass
from src.mrcnn_detect import *

# import cupy as cp
import numpy as np
import matplotlib.pyplot as plt

import logging

logging.getLogger("mrcnn").setLevel(logging.ERROR)


def get_ax(rows=1, cols=1, size=16):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.

    Adjust the size attribute to control how big to render images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
    return ax


# ------------------------------------------------------
# Receipt Dataclass
# ------------------------------------------------------
@dataclass
class Receipt:
    """
    Input:
        - An image binary as a np.ndarray.

    Output:
        - 'mrcnn_detection' attribute:
            Output of detection via mask-rcnn model

        - 'unmasked' attribute:
            Output of element-wise subtraction of mask from the image.

        - 'homography' attribute:
            Output of computed homography for perspective correction.

        - 'corrected_perspective' attribute:
            Output of perspective correction - an image binary as np.ndarray.

        - 'status' attribute:
            An indication of the current processing status. Helps in keeping track of the algorithms.

    ---

    """

    # Instantiates a list of image arrays
    img: np.ndarray

    # ------------------------------------------------------

    def __post_init__(self):

        self.mrcnn_detection = self.mrcnn_detect()
        # self.unmasked = self.unmask()
        # self.homography = self.compute_homography()
        # self.corrected_perspective = self.correct_perspective()
        # self.text_boxes = self.detect_text_boxes()
        # self.text_box_arrangement = self.compute_text_box_arrangement()
        # self.ocr = self.perform_ocr()
        # self.data = self.sanity_check()

    # ------------------------------------------------------

    def mrcnn_detect(self):

        model_path = None
        # mrcnn_detection = model.detect([self.img], verbose=0)[0]

        # Run object detection
        results = model.detect([self.img], verbose=1)

        # Display results
        ax = get_ax(1)
        r = results[0]
        visualize.display_instances(
            self.img,
            r["rois"],
            r["masks"],
            r["class_ids"],
            ["BG", "receipt"],
            r["scores"],
            ax=ax,
            title="Predictions",
        )
        plt.show()
        # print(mrcnn_detection)
        self.status = "[Status] : mrcnn_detect() complete."
        return results

    # ------------------------------------------------------

    def unmask(self):
        """
        Define a simple CUDA kernel for element-wise subtraction

        This way the image masks (m) can be subtracted quickly from the original image (o) to return the result (z).
        """
        # matrix_subtraction = cp.ElementwiseKernel(
        #     'float32 o, float32 m',
        #     'float32 z',
        #     'z = o - m',
        #     'matrix_subtraction'
        # )
        # unmasked = matrix_subtraction(self.img, self.mask)

        self.status = "[Status] : unmask() complete."
        return unmasked

    # ------------------------------------------------------

    def compute_homography(self):
        homography = None

        self.status = "[Status] : compute_homography() complete."
        return homography

    # ------------------------------------------------------

    def correct_perspective(self):
        pass

    # ------------------------------------------------------

    def detect_text_boxes(self):
        pass

    # ------------------------------------------------------

    def get_text_box_arrangement(self):
        pass

    # ------------------------------------------------------

    def perform_ocr(self):
        pass

    # ------------------------------------------------------

    def sanity_check(self):
        pass

    # ------------------------------------------------------

    def plot(self, img):
        """
        Plot can be called at any stage for clarification and interpretation of the processes.

        Usage:

        # Initialize class with image
        >>> R = Receipt(img_array)

        >>> R.plot(R.img)
        >>> R.plot(R.unmasked)
        """
        plt.imshow(img)

    # ------------------------------------------------------


if __name__ == "__main__":
    import cv2

    r = Receipt(
        cv2.imread(
            "/home/yanal/Desktop/Workspace/SimSplit/"
            "simsplit-mrcnn-receipt-detector/customImages/val/1176-receipt.jpg"
        )
    )
