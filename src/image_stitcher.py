#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

__doc__ = """This algorithm essentially takes a list of images, sorted
top-to-bottom, and attempts to stitch them together. Works flawlessly."""

from dataclasses import dataclass
from typing import List
from src.image_stitcher_helpers import (
    detectAndDescribe,
    matchKeyPointsBF,
    matchKeyPointsKNN,
    getHomography,
)
from tempfile import TemporaryDirectory
from uuid import uuid4
from PIL import Image
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
import imutils


@dataclass
class ImageStitcher:
    """Stitcher is an algorithm that using homography and
    feature matching to stitch images together.

    The code is commented step-by-step for a breakdown of
    how it works.

    Args:
        image_paths (:obj:`list` of :obj:`str`): A list of image paths.
        new_image_path (:obj:`str`, optional): Defaults to None, \
        generates an image path.
        show (:obj:`bool`, optional): Defaults to None, Does not show \
        the images upon stitching.

    Examples:
        >>> # Construct a list of image paths
        >>> image_paths=[
                "path/to/image_1.png",
                "path/to/image_2.png",
                "path/to/image_3.png"
            ]
        >>> # Pass the list to the ImageStitcher
        >>> ImageStitcher(image_paths=image_paths)

    """

    # Required Fields
    image_paths: List[str]
    temp_dir: TemporaryDirectory

    # Optional Fields
    new_image_path: str = None
    show: bool = None

    def __post_init__(self):
        temp_dir = self.temp_dir
        # Set new image path
        self.new_image_path = f"{temp_dir.name}/stitched.png"
        new_image_path = self.new_image_path

        # one of 'sift', 'surf', 'brisk', 'orb'
        feature_extractor = "brisk"

        # 'bf' bruteforce or 'knn' k-nearest neighbours
        feature_matching = "bf"

        # Enumerate the image paths into a list of indexed tuples
        paths = self.image_paths
        image_paths = list(enumerate(paths))

        # Get number of images in
        max_n = len(paths)

        # We are performing the first stitch
        first_stitch = True

        # Loop over the images [3]: 0, 1, 2
        for i in range(1, max_n):

            # If this is not the first stitch
            if i > 1:
                # Set the flag
                first_stitch = False

            # If it is the first stitch, set the path to the first in the list.
            if first_stitch:
                top_image_path = image_paths[0][1]
            # Else get the last stitched image
            else:
                top_image_path = new_image_path

            # Set the query image to the top image
            query_img = imageio.imread(top_image_path)

            # Transform to RGB
            query_img_gray = cv2.cvtColor(query_img, cv2.COLOR_RGB2GRAY)

            # Transform the training image to grayscale.
            train_img = imageio.imread(image_paths[i][1])
            train_img_gray = cv2.cvtColor(train_img, cv2.COLOR_RGB2GRAY)

            # Plotting
            if self.show:
                fig, (ax1, ax2) = plt.subplots(
                    nrows=1, ncols=2, constrained_layout=False, figsize=(16, 9)
                )
                ax1.imshow(query_img, cmap="gray")
                ax1.set_xlabel("Query image", fontsize=14)

                ax2.imshow(train_img, cmap="gray")
                ax2.set_xlabel(
                    "Train image (Image to be transformed)", fontsize=14
                )

                plt.show()

            # Detect keypoints using feature extraction method specified above
            kps_a, features_a = detectAndDescribe(
                train_img_gray, method=feature_extractor
            )
            kps_b, features_b = detectAndDescribe(
                query_img_gray, method=feature_extractor
            )

            # Plotting
            if self.show:
                fig, (ax1, ax2) = plt.subplots(
                    nrows=1, ncols=2, figsize=(20, 8), constrained_layout=False
                )
                ax1.imshow(
                    cv2.drawKeypoints(
                        train_img_gray, kps_a, None, color=(0, 255, 0)
                    )
                )
                ax1.set_xlabel("(a)", fontsize=14)
                ax2.imshow(
                    cv2.drawKeypoints(
                        query_img_gray, kps_b, None, color=(0, 255, 0)
                    )
                )
                ax2.set_xlabel("(b)", fontsize=14)

                plt.show()

            print("Using: {} feature matcher".format(feature_matching))

            # if 'b': match Keypoints using bruteforce
            if feature_matching == "bf":
                matches = matchKeyPointsBF(
                    features_a, features_b, method=feature_extractor
                )
                img3 = cv2.drawMatches(
                    train_img,
                    kps_a,
                    query_img,
                    kps_b,
                    matches[:100],
                    None,
                    flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS,
                )

            # if 'knn': match keypoints using k-nearest-neighbour
            elif feature_matching == "knn":
                matches = matchKeyPointsKNN(
                    features_a,
                    features_b,
                    ratio=0.75,
                    method=feature_extractor,
                )
                img3 = cv2.drawMatches(
                    train_img,
                    kps_a,
                    query_img,
                    kps_b,
                    np.random.choice(matches, 100),
                    None,
                    flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS,
                )

            # Plotting
            if self.show:
                plt.figure(figsize=(20, 8))
                plt.imshow(img3)
                plt.show()

            # Get homography between both images
            homography = getHomography(
                kps_a,
                kps_b,
                # features_a,
                # features_b,
                matches,
                reproject_threshold=4,
            )

            # Print an error if there are no matches
            if homography is None:
                print("Error!")
            (matches, H, status) = homography
            print(H)

            # Apply panorama correction
            width = train_img.shape[1] + query_img.shape[1]
            height = train_img.shape[0] + query_img.shape[0]

            # Warp the perspective
            result = cv2.warpPerspective(train_img, H, (width, height))
            result[0 : query_img.shape[0], 0 : query_img.shape[1]] = query_img

            # Plotting
            if self.show:
                plt.figure(figsize=(20, 10))
                plt.imshow(result)
                plt.axis("off")
                plt.show()

            # Transform the panorama image to grayscale and threshold it
            gray = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)
            thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)[1]

            # Plotting
            if self.show:
                plt.figure(figsize=(20, 10))
                plt.imshow(thresh)
                plt.show()

            # Finds contours from the binary image
            contours = cv2.findContours(
                thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
            )
            contours = imutils.grab_contours(contours)

            # Get the maximum contour area
            c = max(contours, key=cv2.contourArea)

            # Get a bbox from the contour area
            (x, y, w, h) = cv2.boundingRect(c)

            # Crop the image to the bbox coordinates
            result = result[y : y + h, x : x + w]

            # Plotting
            if self.show:
                # show the cropped image
                plt.figure(figsize=(20, 10))
                plt.imshow(result)
                plt.show()

            # Save the stitched image.
            Image.fromarray(result).save(new_image_path)

            # # Resize Functionality
            # if image.shape[0] > 2000 or image.shape[1] > 1000:
            #     # Resize with aspect ratio maintained: Expects a tuple
            #     image.thumbnail((1000,1000), Image.ANTIALIAS)
            #     image.save("cropped.jpg", "JPEG")
