#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------


import cv2
import numpy as np
from dataclasses import dataclass
from tempfile import TemporaryDirectory
from src.su_binarization_helpers import nfns, localminmax, numnb, rescale


@dataclass
class SUBinarization:
    """A binarization method that is better than OTSU's Thresholding (for
    our case). Resulting (by accidental discovery) in perfect correction of
    shade. OTSU would instead render any shaded areas in the receipt solid
    black.

    Note:
        Refactored and adapted from GitHub_

        .. _GitHub: https://gist.github.com/pebbie/2c17620e60c662950b\
        02c4949b3010f2

    Examples:
        >>> SUBinarization(filename="path/to/image.png")

    """

    # Required Fields
    filename: str
    temp_dir: TemporaryDirectory

    # Optional Fields
    show: bool = None

    def __post_init__(self):
        temp_dir = self.temp_dir
        # Read image
        image = cv2.imread(self.filename)

        # Conver to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        n_min = 4

        gfn = nfns

        # Normalize from 0-1
        normalized = gray.astype(np.float64)
        if self.show:
            cv2.imshow("I", gray)

        cimg = localminmax(normalized, gfn)
        _, ocimg = cv2.threshold(
            rescale(cimg).astype(gray.dtype),
            0,
            1,
            cv2.THRESH_BINARY + cv2.THRESH_OTSU,
        )
        E = ocimg.astype(np.float64)

        if self.show:
            cv2.imshow("contrast", rescale(cimg, 1.0))
        if self.show:
            cv2.imshow("High contrast", E)

        N_e = numnb(ocimg, gfn)
        nbmask = N_e > 0

        E_mean = np.zeros(normalized.shape, dtype=np.float64)
        for fn in gfn:
            E_mean += fn(normalized) * fn(E)

        E_mean[nbmask] /= N_e[nbmask]

        E_var = np.zeros(normalized.shape, dtype=np.float64)
        for fn in gfn:
            tmp = (fn(normalized) - E_mean) * fn(E)
            E_var += tmp * tmp

        E_var[nbmask] /= N_e[nbmask]
        E_std = np.sqrt(E_var) * 0.5

        R = np.ones(normalized.shape) * 255
        R[(normalized <= E_mean + E_std) & (N_e >= n_min)] = 0
        if self.show:
            cv2.imshow("Result", R)
            cv2.waitKey(0)
        else:
            cv2.imwrite(f"{temp_dir.name}/binarized.png", R)


if __name__ == "__main__":
    # Import the ArgumentSwitcher
    from argument_switcher import ArgumentSwitcher

    # Initialize an instance
    args_switcher = ArgumentSwitcher()
    # Select parser module and pass global __doc__
    parser = args_switcher.select_module(
        module="su_binarization", doc=globals()["__doc__"]
    )
    # Initialize parser arguments
    args = parser.parse_args()

    SUBinarization(**vars(args))
