# -*- coding: utf-8 -*-
"""line_segmentation.py

LineSegmenter is an algorithm, that when given a relatively
low-entropy image, will create contours and segment the image into its
constituent text boxes. It also maps the text boxes by row (and column
if necessary)

"""
#  ---------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  ---------------------------------------------------------------------


import cv2
import numpy as np

import json
import pprint
from dataclasses import dataclass
from tempfile import TemporaryDirectory
from src.grid_maker import GridMaker

PP = pprint.PrettyPrinter(indent=2)


@dataclass
class LineSegmenter:
    """LineSegmenter is an algorithm, that when given a relatively
    low-entropy image, will create contours and segment the image into its
    constituent text boxes. It also maps the text boxes by row (and column
    if necessary)

    Note:
        Refactored and adapted from the StackOverflow Question No. 53962171_

        .. _53962171: https://stackoverflow.com/questions/53962171/\
        perform-line-segmentation-cropping-serially-with-opencv

    Note:
        For better accuracy, we actually use two images, the binarized \
        and the masked images.
            * ``binarized.png`` is used to infer the coordinates of the
            text boxes through line segmentation
            * ``masked.png`` is used to actually extract the regions \
            of interest (ROIs)

    Args:
         file_dict (:obj:`dict`): The paths of the image files
         show (:obj:`bool`, optional): True to plot the images, False to
         skip. Defaults to False.

    Examples:
        >>> LineSegmenter(filename="path/to/image.png")

    """

    file_dict: dict
    temp_dir: TemporaryDirectory = None
    local_dir: str = None
    show: bool = False

    def __post_init__(self):
        binarized_image = self.file_dict["binarized"]
        masked_image = self.file_dict["masked"]

        if self.temp_dir:
            # Assign a local variable to the TemporaryDirectory object
            temp_dir = self.temp_dir

        elif self.local_dir:
            temp_dir = self.local_dir

        # Read the binarized image
        bin_image = cv2.imread(binarized_image)

        # Read the masked image
        msk_image = cv2.imread(masked_image)

        # Convert bin_image to grayscale
        gray = cv2.cvtColor(bin_image, cv2.COLOR_BGR2GRAY)

        # Optional plotting
        if self.show:
            cv2.imshow("gray", gray)

        # Perform binary thresholding
        ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)

        # Optional plotting
        if self.show:
            cv2.imshow("thresholded", thresh)

        # TODO: Choose different kernels based on the image shape.
        #   h >= 750        -> (3, 30)  ~ Default
        #   750 > h >= 500  -> (2, 20)  ~ Smaller
        #   500 > h         -> (1, 10)  ~ Smallest

        # TODO: Choose different kernels based on the image shape.
        #   h >= 750        -> (3, 30)  ~ Default
        #   750 > h >= 500  -> (2, 20)  ~ Smaller
        #   500 > h         -> (1, 10)  ~ Smallest

        # TODO: Reject based on alignment / segmentation

        # Perform dilation
        kernel = np.ones((5, 30), np.uint8)
        img_dilation = cv2.dilate(thresh, kernel, iterations=1)

        # Optional plotting
        if self.show:
            cv2.imshow("dilated", img_dilation)

        # Find contours
        contours, _ = cv2.findContours(
            img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )

        # Sort contours
        sorted_contours = sorted(
            contours, key=lambda contour: cv2.boundingRect(contour)[0]
        )

        # Create an empty list to hold segment (ROI) names.
        self._segment_names = []

        # Create an empty dictionary to hold the segmentation data.
        seg_data = {}
        segment_dict = []

        # Loop over the sorted contours
        for i, ctr in enumerate(sorted_contours):

            # Get bounding box
            x, y, w, h = cv2.boundingRect(ctr)

            # Getting ROI
            roi = msk_image[y : y + h, x : x + w]

            # Sometimes a contour spanning the whole image is detected
            # - particularly if the image is not binarized, it needs to
            # be removed. So we check if the region of interest (the
            # detected text box) has the same x,y shape as the original
            # image, i.e. it spans the entire image (so it is not really
            # a detection).
            if roi.shape != msk_image.shape:

                # Calculate centroid (x, y)
                centroid = self.compute_centroid(x, y, w, h)

                # Collect centroid and coordinates
                seg_data[i] = {
                    # Format is (x, y, w, h)
                    "coords": (x, y, w, h),
                    "centroid": centroid,
                }
                segment_dict.append(seg_data[i])

                # Saves the ROI
                segment_fname = f"{temp_dir.name}/segment_no_{i}.png"
                self._segment_names.append(f"segment_no_{i}.png")

                # Record segment filename (for later use)
                seg_data[i]["filename"] = segment_fname

                # Write segment to file
                cv2.imwrite(segment_fname, roi)

                # Draw the segment rectangle on the original image
                cv2.rectangle(
                    msk_image, (x, y), (x + w, y + h), (139, 70, 0), 2
                )

        # Create an instance of gridmaker
        gm = GridMaker(segment_dict=segment_dict)

        # Save the row_collation property to json
        with open(f"{temp_dir.name}/detections.json", "w") as f:
            json.dump(gm.row_collation, f)

        # Save the image with boxes to file
        cv2.imwrite(f"{temp_dir.name}/boxes.png", msk_image)

        # Optional Plotting
        if self.show:
            cv2.imshow("marked areas", msk_image)
            cv2.waitKey(0)
            # cv2.destroyAllWindows()

    @property
    def segment_names(self):
        return self._segment_names

    @staticmethod
    def compute_centroid(x, y, w, h):
        """Computes the centroid of a rectangle as (x, y) integer
        coordinates
        """
        x0 = x
        y0 = y
        x1 = x + w
        y1 = y + h

        centroid = ((x0 + x1) // 2, (y0 + y1) // 2)
        return centroid


if __name__ == "__main__":
    pass
