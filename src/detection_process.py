#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

# For webhooks
import requests

# For json data
import json

# Import Receipt Detection Algorithm
from src.receipt_detectron import ReceiptDetectron

# Import reader
import cv2


# ------------------------------------------------------------------------
# ReceiptDetectionProcess
# ------------------------------------------------------------------------
class ReceiptDetectionProcess:
    """This class contains the following methods:

    Core methods:
        1. __init__()
            - initializes the arguments
        2. run()
            - runs the processes

    Extra methods:
        1. run_detection()
    """

    def __init__(self, h_args, args):
        self.h_args = h_args
        self.args = args

    def run(self):
        try:
            print("--- [START] SimSplit OCR Detection Process ---")

            # # Load current webhooks
            # with open('webhooks.json') as f:
            #     W = json.load(f)

            # Run Processes
            f1 = self.run_detection(**self.args)
            self.status = all([f1])
        except Exception as e:
            self.status = False
        finally:
            # url = W['rank']
            # headers = {'Content-Type': 'application/json'}
            # response = requests.post(
            #     url,
            #     headers = headers,
            #     json =  {
            #         "identifiers": {
            #             **self.h_args
            #             },
            #          "results": self.results
            #     },
            #     params = {
            #         "status": self.status
            #     }
            # )

            print("--- [END] SimSplit OCR Detection Process ---")

    def run_detection(self, **kwargs):
        # Assign keyword arguments
        image_paths = kwargs["image_paths"]

        # Try Statement
        try:
            ReceiptDetectron(image_paths=image_paths)
            return 1

        # Catch Statement
        except Exception as e:
            print(e)
            return 0
