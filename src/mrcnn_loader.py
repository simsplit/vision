#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

# General
import os
import tensorflow as tf

# Mask RCNN
import mrcnn.model as modellib

# Receipt custom class
from .receipt import ReceiptConfig

# Import env vars
from settings import (
    MRCNN_DEVICE,
    MRCNN_MODE,
    MRCNN_WEIGHTS_PATH,
    MRCNN_MODEL_PATH,
)

# Since we are not going to port this to TF2.0, we are going to ignore the
# deprecation errors
import logging

logging.getLogger("tensorflow").setLevel(logging.ERROR)

# Configure Receipt and Inference
config = ReceiptConfig()


class InferenceConfig(config.__class__):
    # Run detection on one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


config = InferenceConfig()
config.display()

# Initialize the model in Inference Mode
with tf.device(MRCNN_DEVICE):
    model = modellib.MaskRCNN(
        mode=MRCNN_MODE, config=config, model_dir=MRCNN_MODEL_PATH
    )


# Load the model weights
model.load_weights(MRCNN_WEIGHTS_PATH, by_name=True)
