# -*- coding: utf-8 -*-
#  ---------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  ---------------------------------------------------------------------

from pydantic import BaseModel
from typing import List, Dict
from uuid import UUID, uuid4


# One Image
class BaseImageRequestSingle(BaseModel):
    """BaseImageRequestSingle

    Args:
        bucket_name (:obj:`str`)
        user_uuid (:obj:`UUID`)
        receipt_uuid (:obj:`UUID`)
        filename (:obj:`str`)

    Returns:
        :class:`BaseImageRequestSingle`
    """

    bucket_name: str
    receipt_uuid: UUID
    filename: str


# Many Images
class BaseImageRequestMany(BaseModel):
    """BaseImageRequestMany

    Args:
        bucket_name (:obj:`str`)
        user_uuid (:obj:`UUID`)
        receipt_uuid (:obj:`UUID`)
        filename (:obj:`list` of :obj:`str`)

    Returns:
        :class:`BaseImageRequestMany`
    """

    bucket_name: str
    receipt_uuid: UUID
    filenames: List[str]


# Image Stitching
class ImageStitchingRequest(BaseImageRequestMany):
    """ImageStitchingRequest

    Args:
        uuid (:obj:`UUID`)
        image_paths (:obj:`list` of :obj:`str`)

    Returns:
        :class:`ImageStitchingRequest`
    """

    pass


# Blur Detection
class BlurDetectionRequest(BaseImageRequestSingle):
    """BlurDetectionRequest

    Args:
        threshold (:obj:`int`, optional): Defaults to 150 (calibrated \
        for real-life images of receipts.)

    Returns:
        :class:`BlurDetectionRequest`
    """

    threshold: int = 150


# Binarization
class BinarizationRequest(BaseImageRequestSingle):
    """BinarizationRequest

    Returns:
        :class:`BinarizationRequest`
    """

    pass


# Receipt Detectron
class ReceiptDetectronRequest(BaseImageRequestSingle):
    """ReceiptDetectronRequest

    Returns:
        :class:`ReceiptDetectronRequest`
    """

    pass


# Receipt OCR
class ReceiptOCRRequest(BaseModel):
    """ReceiptOCRRequest

    Args:
        bucket_name (:obj:`str`)
        user_uuid (:obj:`UUID`)
        receipt_uuid (:obj:`UUID`)
        file_dict (:obj:`dict` of :obj:`str`,:obj:`str`)

    Returns:
        :class:`ReceiptOCRRequest`
    """

    bucket_name: str
    receipt_uuid: UUID
    file_dict: Dict[str, str]
