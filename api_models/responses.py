#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

from pydantic import BaseModel
from uuid import UUID


responses = {
    200: {"description": "Successful request."},
    404: {"description": "Item not found"},
    500: {"description": "Server-side Error (Internal Server Error)"},
}


# Health Response model
class HealthResponse(BaseModel):
    """HealthResponse

    Args:
        status (:obj:`int`): Defaults to 200

    Returns:
        :class:`HealthResponse`
    """

    status: int = 200


# Standard Response
class StandardResponse(BaseModel):
    """HealthResponse

    Args:
        uuid (:obj:`UUID`): The UUID passed in the request (user)
        status (:obj:`int`): Defaults to 200
        msg (:obj:`str`): Defaults to 'Success'
        body (:obj:`dict`): Defaults to None

    Returns:
        :class:`StandardResponse`
    """

    status: int = 200
    msg: str = "Success"
    body: dict = None
