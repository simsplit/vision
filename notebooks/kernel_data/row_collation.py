# -*- coding: utf-8 -*-
var = {
    0: [
            {
                'coords': (284, 33, 193, 8),
                'centroid': (380, 37),
                'filename': './tmpw8hokfzb/segment_no_29.png'
            },
            {
                'coords': (105, 36, 62, 21),
                'centroid': (136, 46),
                'filename': './tmpw8hokfzb/segment_no_12.png'
            }
    ],
    1: [
            {
                'coords': (214, 118, 141, 41),
                'centroid': (284, 138),
                'filename': './tmpw8hokfzb/segment_no_28.png'
            }
    ],
    2: [
            {
                'coords': (201, 155, 163, 63),
                'centroid': (282, 186),
                'filename': './tmpw8hokfzb/segment_no_26.png'
            }
    ],
    3: [
            {
                'coords': (203, 217, 161, 28),
                'centroid': (283, 231),
                'filename': './tmpw8hokfzb/segment_no_27.png'
            }
    ],
    4: [
            {
                "coords": (31, 232, 125, 26),
                'centroid': (93, 245),
                'filename': './tmpw8hokfzb/segment_no_2.png'
            }
    ],
    5: [
            {
                'coords': (339, 253, 133, 21),
                'centroid': (405, 263),
                'filename': './tmpw8hokfzb/segment_no_31.png'
            },
            {
                'coords': (30, 260, 168, 28),
                'centroid': (114, 274),
                'filename': './tmpw8hokfzb/segment_no_1.png'
            }
    ],
    6: [
            {
                'coords': (286, 276, 195, 24),
                'centroid': (383, 288),
                'filename': './tmpw8hokfzb/segment_no_30.png'
            }
    ],
    7: [
            {
                'coords': (63, 290, 197, 29),
                'centroid': (161, 304),
                'filename': './tmpw8hokfzb/segment_no_9.png'
            }
    ],
    8: [
            {
                'coords': (391, 307, 89, 20),
                'centroid': (435, 317),
                'filename': './tmpw8hokfzb/segment_no_36.png'
            }
    ],
    9: [
            {
                'coords': (115, 320, 124, 25),
                'centroid': (177, 332),
                'filename': './tmpw8hokfzb/segment_no_18.png'
            }
    ],
    10: [
            {
                'coords': (114, 348, 134, 23),
                'centroid': (181, 359),
                'filename': './tmpw8hokfzb/segment_no_17.png'
            }
    ],
    11: [
            {
                'coords': (112, 374, 157, 25),
                'centroid': (190, 386),
                'filename': './tmpw8hokfzb/segment_no_16.png'
            }
    ],
    12: [
            {
                'coords': (110, 401, 136, 24),
                'centroid': (178, 413),
                'filename': './tmpw8hokfzb/segment_no_15.png'
            }
    ],
    13: [
            {
                'coords': (110, 429, 220, 27),
                'centroid': (220, 442),
                'filename': './tmpw8hokfzb/segment_no_14.png'
            }
    ],
    14: [
            {
                'coords': (109, 456, 209, 27),
                'centroid': (213, 469),
                'filename': './tmpw8hokfzb/segment_no_13.png'
            },
            {
                'coords': (397, 466, 79, 22),
                'centroid': (436, 477),
                'filename': './tmpw8hokfzb/segment_no_38.png'
            }
    ],
    15: [
            {
                'coords': (194, 515, 108, 23),
                'centroid': (248, 526),
                'filename': './tmpw8hokfzb/segment_no_25.png'
            },
            {
                'coords': (385, 522, 90, 22),
                'centroid': (430, 533),
                'filename': './tmpw8hokfzb/segment_no_34.png'
            }
    ],
    16: [
            {
                'coords': (193, 542, 60, 21),
                'centroid': (223, 552),
                'filename': './tmpw8hokfzb/segment_no_24.png'
            },
            {
                'coords': (395, 549, 79, 21),
                'centroid': (434, 559),
                'filename': './tmpw8hokfzb/segment_no_37.png'
            }
    ],
    17: [
            {
                'coords': (192, 571, 76, 37),
                'centroid': (230, 589),
                'filename': './tmpw8hokfzb/segment_no_23.png'
            },
            {
                'coords': (383, 577, 89, 36),
                'centroid': (427, 595),
                'filename': './tmpw8hokfzb/segment_no_33.png'
            }
    ],
    18: [
            {
                'coords': (189, 637, 71, 37),
                'centroid': (224, 655),
                'filename': './tmpw8hokfzb/segment_no_22.png'
            },
            {
                'coords': (381, 642, 91, 37),
                'centroid': (426, 660),
                'filename': './tmpw8hokfzb/segment_no_32.png'
            }
    ],
    19: [
            {
                'coords': (188, 677, 92, 42),
                'centroid': (234, 698),
                'filename': './tmpw8hokfzb/segment_no_21.png'
            },
            {
                'coords': (391, 682, 79, 36),
                'centroid': (430, 700),
                'filename': './tmpw8hokfzb/segment_no_35.png'
            }
    ],
    20: [
            {
                'coords': (27, 746, 470, 19),
                'centroid': (262, 755),
                'filename': './tmpw8hokfzb/segment_no_0.png'
            }
    ],
    21: [
            {
                'coords': (48, 774, 427, 18),
                'centroid': (261, 783),
                'filename': './tmpw8hokfzb/segment_no_8.png'
            }
    ],
    22: [
            {
                'coords': (46, 810, 38, 23),
                'centroid': (65, 821),
                'filename': './tmpw8hokfzb/segment_no_7.png'
            },
            {
                'coords': (157, 802, 210, 40),
                'centroid': (262, 822),
                'filename': './tmpw8hokfzb/segment_no_20.png'
            },
            {
                'coords': (439, 814, 38, 22),
                'centroid': (458, 825),
                'filename': './tmpw8hokfzb/segment_no_39.png'
            }
    ],
    23: [
            {
                'coords': (45, 852, 38, 22),
                'centroid': (64, 863),
                'filename': './tmpw8hokfzb/segment_no_6.png'
            },
            {
                'coords': (123, 844, 266, 40),
                'centroid': (256, 864),
                'filename': './tmpw8hokfzb/segment_no_19.png'
            },
            {
                'coords': (440, 855, 37, 22),
                'centroid': (458, 866),
                'filename': './tmpw8hokfzb/segment_no_41.png'
            }
    ],
    24: [
            {
                'coords': (44, 887, 38, 13),
                'centroid': (63, 893),
                'filename': './tmpw8hokfzb/segment_no_5.png'
            },
            {
                'coords': (440, 890, 37, 13),
                'centroid': (458, 896),
                'filename': './tmpw8hokfzb/segment_no_40.png'
            }
    ],
    25: [
            {
                'coords': (42, 917, 38, 13),
                'centroid': (61, 923),
                'filename': './tmpw8hokfzb/segment_no_4.png'
            },
            {
                'coords': (88, 913, 347, 23),
                'centroid': (261, 924),
                'filename': './tmpw8hokfzb/segment_no_11.png'
            },
            {
                'coords': (441, 919, 37, 14),
                'centroid': (459, 926),
                'filename': './tmpw8hokfzb/segment_no_42.png'
            }
    ],
    26: [
            {
                'coords': (42, 946, 38, 13),
                'centroid': (61, 952),
                'filename': './tmpw8hokfzb/segment_no_3.png'
            },
            {
                'coords': (87, 943, 337, 26),
                'centroid': (255, 956),
                'filename': './tmpw8hokfzb/segment_no_10.png'
            },
            {
                'coords': (442, 949, 36, 14),
                'centroid': (460, 956),
                'filename': './tmpw8hokfzb/segment_no_43.png'
            }
    ]
}
