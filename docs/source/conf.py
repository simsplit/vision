# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from datetime import datetime
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath("../.."))


# -- Project information -----------------------------------------------------

project = "SimSplit Vision"
author = "SimSplit Co."
version = "latest"
release = "1.0.0rc"
copyright = f"{datetime.now().year}, SimSplit Co."

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_rtd_theme",
    "sphinx.ext.napoleon",
    "sphinx.ext.autodoc",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx.ext.coverage",
    "sphinx.ext.extlinks",
]
extlinks = {
    # 'issue': ('https://github.com/sphinx-doc/sphinx/issues/%s', 'issue')
    "lineseg": (
        "https://stackoverflow.com/questions/53962171/perform-line"
        "-segmentation-cropping-serially-with-opencv",
        "lineseg",
    )
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# Code styling
pygments_style = "sphinx"

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# Imports the Read the Docs theme
html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# Displays Logo in sidebar
# html_logo = 'logo.jpg'

# Theme options
html_theme_options = {
    "canonical_url": "",
    # 'analytics_id': 'UA-XXXXXXX-1',  #  Provided by Google in your dashboard
    # 'logo_only': False,
    "display_version": True,
    # 'prev_next_buttons_location': 'bottom',
    "style_external_links": True,
    # 'vcs_pageview_mode': '',
    # 'style_nav_header_background': '#03396c',
    # Toc options
    "collapse_navigation": False,
    "sticky_navigation": True,
    "navigation_depth": 4,
    "includehidden": True,
    "titles_only": False,
}

# -- LaTeX configs -------------------------------------------------
# latex_engine = 'xelatex'
# latex_elements = {
#     'fontpkg': r'''
# \setmainfont{Ubuntu}
# \setsansfont{Ubuntu}
# \setmonofont{Ubuntu Mono}
# ''',
#     'preamble': r'''
# \usepackage[utf8]{inputenc}
# '''
# }
