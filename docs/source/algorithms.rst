.. _algorithms:

Image Processing Algorithms
-----------------------------

All image processing algorithms are documented here.


.. _topics-blur-detector:

The ``BlurDetector`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: src.blur_detection.BlurDetector
    :members:
    :show-inheritance:


.. _topics-image-stitcher:

The ``ImageStitcher`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: src.image_stitcher.ImageStitcher
    :members:
    :show-inheritance:


.. _topics-line-segmenter:

The ``LineSegmenter`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: src.line_segmentation.LineSegmenter
    :members:
    :show-inheritance:


.. _topics-su-binarization:

The ``SUBinarization`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: src.su_binarization.SUBinarization
    :members:
    :show-inheritance:


