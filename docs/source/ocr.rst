.. _algorithms-ocr:

OCR Algorithms
-----------------------------

All image processing algorithms are documented here.

.. _topics-ocr:

The ``OCR`` class (for receipts)
*********************************

.. autoclass:: src.ocr.OCR
    :members:
    :show-inheritance:
