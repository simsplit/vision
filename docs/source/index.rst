.. SimSplit Vision documentation master file, created by
   sphinx-quickstart on Mon Apr 13 15:23:25 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SimSplit Vision |release| documentation
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Algorithms
=============================

.. toctree::
   :caption: Algorithms
   :hidden:

   algorithms
   ocr

:doc:`algorithms`
   Image Processing Algorithms

:doc:`ocr`
   Optical Character Recognition


API Models
=============================

.. toctree::
   :caption: API Models
   :hidden:

   request_models
   response_models

:doc:`request_models`
   Pydantic request validation models.

:doc:`response_models`
   Pydantic response validation models.

Helpers and Miscellaneous Functions
=====================================

.. toctree::
   :caption: Misc
   :hidden:

   miscellaneous

:doc:`miscellaneous`
   Misc Functions