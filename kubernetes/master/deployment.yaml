apiVersion: apps/v1

# Multi-pod deployment
kind: Deployment
metadata:
  labels:
    app: simsplit-vision
  name: simsplit-vision
  namespace: default
spec:
  replicas: 3
  selector:
    matchLabels:
      app: simsplit-vision
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: simsplit-vision
    spec:
      # Use the nodeSelector
      nodeSelector:
        component: vision
      containers:
        # Container 1:
        # relay-manager
        # This is a side-car API made specifically to provide information
        # to seamlessly relay traffic between pods in a cluster based
        # on regional and zonal distance.
        # Expose port 7900 (hopefully to become a standard)
        - image: gcr.io/staging-sim/tools/relay-manager:master
          imagePullPolicy: Always
          name: relay-manager
          ports:
          - containerPort: 7900

          # Define health check route
          livenessProbe:
            httpGet:
              path: /healthz
              port: 7900
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1
          readinessProbe:
            httpGet:
              path: /healthz
              port: 7900
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1

          # Inherit Environment Variables
          envFrom:
          - configMapRef:
              name: env-variables

        # Container 2:
        # vision
        # This is the main app. A complete computer vision system with image
        # processing functions, detectrons and and OCR.
        # Expose port 8000
        - image: gcr.io/staging-sim/ai/vision:master
          imagePullPolicy: Always
          name: vision
          ports:
          - containerPort: 8000

          # We can define what happens as the container starts and stops
          lifecycle:
            # When the container starts, we want to run a script to register the pod
            # by sending a hook to the other container 'relay-manager'
            postStart:
              exec:
                command: [
                    "/bin/sh", "-c", "/etc/pod-reg/register_pod.sh"
                ]

            # When the container stops, we want to run a script to deregister the pod
            # by sending a hook to the other container 'relay-manager'
            preStop:
              exec:
                command: [
                    "/bin/sh", "-c", "/etc/pod-dereg/deregister_pod.sh"
                ]

          # Define health check route
          livenessProbe:
            httpGet:
              path: /healthz
              port: 8000
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1
          readinessProbe:
            httpGet:
              path: /healthz
              port: 8000
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 1

          # Inherit Environment Variables
          envFrom:
          - configMapRef:
              name: env-variables

          # Service account credentials
          env:
          - name: GOOGLE_APPLICATION_CREDENTIALS
            value: /etc/gcp/sa_credentials.json

          # Mount voulumes
          volumeMounts:
          # Mount service account credential volume
          - name: service-account-credentials-volume
            mountPath: /etc/gcp
            readOnly: true
          # Mount pod registration volume
          - name: register-pod
            mountPath: /etc/pod-reg
          # Mount pod deregistration volume
          - name: deregister-pod
            mountPath: /etc/pod-dereg

      # Define volumes
      volumes:
      # Create a credentials volume using the secret
      - name: service-account-credentials-volume
        secret:
          secretName: compute-service-account-credentials
          items:
          - key: secret.json
            path: sa_credentials.json

      # Create ConfigMap volume for pod registration
      - name: register-pod
        configMap:
          name: pod-registration-configmap
          defaultMode: 0744

      # Create ConfigMap volume for pod deregistration
      - name: deregister-pod
        configMap:
          name: pod-deregistration-configmap
          defaultMode: 0744
