# OCR Prototype

This is an algorithm designed to take a real-life image of a receipt, process it and finally extract relevant information from it.

## The Pipeline

1. **MRCNN Mask Extraction** -
Extracting the mask and normalizing the cropped region removed all external noise that may screw up the following steps.

2. **SU Binarization** -
A binarization method that is better than OTSU's Thresholding. Resulting (by accidental discovery) in perfect correction of shade. OTSU would instead render any shaded areas in the receipt solid black.

3. **Line Segmentation** -
This method requires a clean, aligned image. Cleaning and preparation were done in the previous steps. Alignment is trickier, as receipts are generally in a grid-based format, this would cause alignment to correct the degree within the same quadrant, essentially sometimes causing full pi/2 rotations.
Either way, this works better than deep learning text boxes. However, a logic must be written to map the boxes to a grid, which is the receipt layout itself.

4. **Grid Mapping** -
Inside the Line Segmentation algorithm, we implement the Grid Mapping
 algorithm where we attempt to map each detected box (segment) into a grid of
  rows and columns. We compute the centroid of each segment (_ROI_, _bounding box_), after which, we perform a heuristic to check each centroid, if it lies within the y-coordinates of any of the other boxes then it is flagged as a row pair. (i.e. it lies on the same row.)
The same process is implemented for the columns, however, it is less discriminatory than the rows as the no. of rows > no. of cols. This results in a `data.json` file, containing all data necessary for OCR in the next stage.

5. **OCR** -
After grid-mapping, we apply OCR to each segment. This process consumes a bit of time because of the number of images. The `pytesseract` module is used in this case, with the mode set to 6, which gave the best results after testing on a some images. The data then is analyzed through multiple methods where it is cleaned, corrected, checked if it is numeric, alphanumeric, alphabetical, a date, a url or a price. This part can be further improved with more careful heuristics. It then collects all the data and updates the `data.json` file accordingly.

## Extras

1. **Image Stitching** -
This algorithm essentially takes a list of images, sorted top-to-bottom, and attempts to stitch them together. Works flawlessly.

2. **Blurry Image Check** -
This algorithms calculates the variance of the Laplacian of the image. We can flag images with variance < threshold as _blurry_. However, this is dataset-dependent and not all thresholds will work well. Still, after careful observation of around 200 real-life receipt images, we arrive at the conclusion that a threshold set at t=150, is quite discriminatory and serves our purpose well.

## Future Ideas

1. **_Perspective Correction of Polygons_** _(Advanced)_- This can be used to ensure the image topology is properly transformed. This would correct curves in the text itself as a consequence, resulting in something similar to a printed paper. It was inspired from 4-point perspective correction, however, we can extend this, to split a polygon into _N_-different blocks, and perform _N_-different 4-point perspective correction iteratively and then finally concatenate the resulting arrays into one image.

## API Routes

__API_VERSION:__ `api/v1/ocr-prototype`

You can find the routes of the API listed below.

1. __`$API_VERSION/detectron`__
This route runs the entire pipeline on an image, from input to OCR.
2. __`$API_VERSION/algorithms/stitcher`__
This route accepts multiple images (sorted top-to-bottom) and stitches them
 together.
3. __`$API_VERSION/algorithms/blur_detector`__
This route accepts an image and determines whether it is blurry or not.
4. __`$API_VERSION/algorithms/perpective_corrector`__
This route accepts an image and performs perspective correction.
5. __`$API_VERSION/algorithms/ocr`__
This route accepts an image and runs OCR detection.

## Folder Structure

```
.
├── app.py
├── config.yaml
├── Dockerfile
├── docker-provision.sh
├── env
├── __init__.py
├── models
├── README.md
├── requirements.txt
├── src
└── tmp

4 directories, 7 files
```

## Deployment

Image building is automated using Google Cloud Build. However, to build the
 image and run the container, we can simply run the following commands:

```
# Build image(s)
docker build -t ocr-prototype .

# Run image(s)
docker run -it ocr-prototype
```

## Usage

### Blur Detection
```
$ python src/blur_detection.py --help
usage: blur_detection.py [-h] [-i INPUT_PATH] [-f FILENAME] [-t THRESHOLD]
                         [--show]

Blur Detector is an algorithm that classifies images as blurry or not by
thresholding the variance of the Laplacian of the image.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_PATH, --input_path INPUT_PATH
                        folder containing image file
  -f FILENAME, --filename FILENAME
                        name of image file
  -t THRESHOLD, --threshold THRESHOLD
                        discrimination threshold (varies with dataset)
  --show                show resulting image plots
```

### Image Stitcher
```
$ python src/image_stitcher.py --help
usage: image_stitcher.py [-h] [-i INPUT_PATH] [-o OUTPUT_PATH]
                         [-f FILENAMES [FILENAMES ...]] [--show]

This algorithm essentially takes a list of images, sorted
top-to-bottom, and attempts to stitch them together. Works flawlessly.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_PATH, --input_path INPUT_PATH
                        folder containing image file
  -o OUTPUT_PATH, --output_path OUTPUT_PATH
                        folder to output image file
  -f FILENAMES [FILENAMES ...], --filenames FILENAMES [FILENAMES ...]
                        name of image files (within same directory)
  --show                show resulting image plots
```

### SU Binarization
```
$ python src/su_binarization.py --help
usage: su_binarization.py [-h] [-i INPUT_PATH] [-o OUTPUT_PATH] [-f FILENAME]
                          [--show]

A binarization method that is better than OTSU's Thresholding. Resulting (by
accidental discovery) in perfect correction of shade. OTSU would instead
render any shaded areas in the receipt solid black.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_PATH, --input_path INPUT_PATH
                        folder containing image file
  -o OUTPUT_PATH, --output_path OUTPUT_PATH
                        folder to output image file
  -f FILENAME, --filename FILENAME
                        name of image file
  --show                show resulting image plots
```

### Line Segmentation and Grid Mapping
```
$ python src/line_segmentation.py --help
usage: line_segmentation.py [-h] [-i INPUT_PATH] [-s SEGMENTS_PATH]
                            [-b BOXES_PATH] [-f FILENAME] [--show]

This is the line segmentation and grid-mapping algorithm, that when given a
relatively low-entropy image, will create contours and segment the image
into its constituent text boxes. It also maps the text boxes by row (and 
column if necessary)

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_PATH, --input_path INPUT_PATH
                        folder containing image file
  -s SEGMENTS_PATH, --segments_path SEGMENTS_PATH
                        folder to output image segments
  -b BOXES_PATH, --boxes_path BOXES_PATH
                        folder to output text-box detections
  -f FILENAME, --filename FILENAME
                        name of image file
  --show                show resulting image plots
```

### OCR
```
$ python src/ocr.py --help
usage: ocr.py [-h] filename

OCR: Our main OCR is an implementation of Tesseract 4 and relies heavily on
sane and sound pre-processing of the images. It requires not an image, but a
.json mapping of image files.

positional arguments:
  filename    name of .json mapping

optional arguments:
  -h, --help  show this help message and exit
```