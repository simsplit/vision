# -*- coding: utf-8 -*-
"""settings.py

This module contains the standard :class:`Settings` class that parses
variables imported from ``app_config.py`` into easily accessible properties.

"""
# For the Settings
import socket
import requests

# Fast api logger
from fastapi.logger import logger

# Import mongo db client
from pymongo import MongoClient

# Catch client error
from requests.exceptions import ConnectionError

# Import app configuration
from .app_config import APP_CONFIG

# To generate a persistent id for the pod
from bson import ObjectId

# For the MRCNN model
# General
import os

# Import tensorflow
import tensorflow as tf

# Mask RCNN
import mrcnn.model as modellib

# Receipt custom class
from src.receipt import ReceiptConfig

# Since we are not going to port this to TF2.0, we are going to ignore
# the deprecation errors
import logging

logging.getLogger("tensorflow").setLevel(logging.ERROR)


class Singleton(type):
    """A singleton metaclass"""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs
            )

        return cls._instances[cls]


class MRCNN(metaclass=Singleton):
    def __init__(self, app_config: dict):
        self.cfg = app_config

    def model_loader(self):
        # Configure Receipt and Inference
        config = ReceiptConfig()

        class InferenceConfig(config.__class__):
            # Run detection on one image at a time
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1

        config = InferenceConfig()
        config.display()

        # Initialize the model in Inference Mode
        with tf.device(self.__device):
            model = modellib.MaskRCNN(
                mode=self.__mode, config=config, model_dir=self.__model_path
            )
        global graph
        # Load the model weights
        model.load_weights(self.__weights_path, by_name=True)
        graph = tf.get_default_graph()

        return model, graph

    @property
    def cloud_blob(self):
        return self.cfg["cloud_blob"]

    @property
    def __device(self):
        return self.cfg["device"]

    @property
    def __mode(self):
        return self.cfg["mode"]

    @property
    def __root_dir(self):
        return self.cfg["root_dir"]

    @property
    def __weights_path(self):
        return self.cfg["weights_path"]

    @property
    def __model_path(self):
        return self.cfg["model_path"]


class Settings:
    """``Settings`` is a class that initializes all settings as
    accessible properties.
    """

    def __init__(self, app_config: dict):
        """
        Args:
            app_config (:obj:`dict`): A dictionary of configurations,
            usually saved in `src.app_config.py`
        """
        self.cfg = app_config
        # self.mongo_client = self.create_mongo_client()
        self.mrcnn = MRCNN(self.cfg["ai"]["mrcnn"])
        self.pod_id = ObjectId()
        # self.mrcnn_model = mrcnn.model_loader()
        # self.cloud_blob = mrcnn.cloud_blob

    @property
    def environment(self):
        return self.cfg["general"]["environment"]

    @property
    def api_version(self):
        return self.cfg["general"]["api_version"]

    @property
    def webhook_id(self):
        return self.cfg["general"]["webhook_id"]

    @property
    def pod_metadata(self):
        """A command to run in a subprocess

        Returns:
            region (:obj:`str`):
            zone (:obj:`str`):
        """
        __request_dict = self.cfg["relay"]["pod_location_request"]

        __pod_metadata = {}
        __pod_region = ""
        __pod_zone = ""
        __pod_id = self.pod_id
        __pod_name = socket.gethostname()
        __pod_addr = socket.gethostbyname(__pod_name)

        try:
            # Pass unpacked dictionary and make a request
            response = requests.get(**__request_dict)

            # if OK
            if response.status_code == 200:

                # 'projects/************/zones/europe-west4-b'
                __response_text = response.text
                # 'europe-west4-b'
                __text = __response_text.split("/")[-1]
                # ['europe', 'west4', 'b']
                __text_breakdown = __text.split("-")
                # 'europe-west4'
                __region = "-".join(__text_breakdown[:2])
                # 'b'
                __zone = __text[-1]

        except ConnectionError as e:
            logger.error(e)

        finally:
            __pod_metadata = dict(
                _id=__pod_id,
                name=__pod_name,
                addr=__pod_addr,
                region=__pod_region,
                zone=__pod_zone,
                availability=1,  # 1 is available, 0 is busy
            )

        return __pod_metadata

    @property
    def path_exemptions(self):
        """

        Returns:

        """
        return self.cfg["relay"]["path_exemptions"]

    def create_mongo_client(self):
        """Connects to a MongoDB

        Returns:
            mongo_uri (:obj:`str`): A standard MongoDB URI Connection \
            string.
        """
        # Get credentials
        _user = self.cfg["databases"]["mongo_admin"]["user"]
        _pass = self.cfg["databases"]["mongo_admin"]["pass"]
        _addr = self.cfg["databases"]["mongo_admin"]["addr"]
        _port = self.cfg["databases"]["mongo_admin"]["port"]

        # Contsruct URI
        __mongo_uri = f"mongodb://{_user}:{_pass}@{_addr}:{_port}"

        # Initialize a client
        __mongo_client = MongoClient(__mongo_uri)

        return __mongo_client

    @property
    def max_jumps(self):
        """

        Returns:

        """
        return self.cfg["relay"]["max_jumps"]

    # @property
    # def pod_registrar_cl(self):
    #     """
    #
    #     Returns:
    #
    #     """
    #     return self.mongo_client['simsplit']['pod_registrar']
    #
    # @property
    # def relay_analysis_cl(self):
    #     """
    #
    #     Returns:
    #
    #     """
    #     return self.mongo_client['simsplit']['relay_analysis']

    @property
    def models_bucket(self):
        return self.cfg["ai"]["models_bucket"]


# Initialize a Settings object
settings = Settings(app_config=APP_CONFIG)


if __name__ == "__main__":
    pass
