# -*- coding: utf-8 -*-
"""app_config.py

This module contains the :obj:`APP_CONFIG` dictionary which controls all
aspects of the app, such as:

    * general settings
    * mongodb data/admin settings
    * host connectivity

"""
import os

APP_CONFIG = dict(
    # General Settings
    general={
        "environment": os.getenv("ENVIRONMENT"),
        "api_version": os.getenv("SIMSPLIT_VISION_API_VERSION"),
        "webhook_id": os.getenv("SIMSPLIT_VISION_WEBHOOK_ID"),
    },
    # Relay middleware (chaos relay)
    relay={
        "path_exemptions": ["/", "/healthz"],
        "max_jumps": 10,
        # Get pod region + zone
        "pod_location_request": {
            "url": "http://metadata.google.internal/"
            "computeMetadata/v1/instance/zone",
            "headers": {"Metadata-Flavor": "Google"},
        },
    },
    # Databases
    databases={
        # Mongo Administrator
        "mongo_admin": {
            "user": os.getenv("MONGO_ADMIN_USER"),
            "pass": os.getenv("MONGO_ADMIN_PASS"),
            "addr": os.getenv("MONGO_ADMIN_ADDR"),
            "port": os.getenv("MONGO_ADMIN_PORT"),
        },
    },
    ai={
        "models_bucket": os.getenv("MODELS_BUCKET"),
        "mrcnn": {
            "cloud_blob": os.getenv("MRCNN_MODEL_CLOUD_BLOB"),
            "device": os.getenv("MRCNN_DEVICE"),
            "mode": os.getenv("MRCNN_MODE"),
            "root_dir": os.getenv("MRCNN_ROOT_DIR"),
            "weights_path": os.getenv("MRCNN_WEIGHTS_PATH"),
            "model_path": os.getenv("MRCNN_MODEL_PATH"),
        },
    },
)
