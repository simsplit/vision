# -*- coding: utf-8 -*-
title = "SimSplit Vision API"

version = "1.0.0"

description = """
# Overview
Welcome to the **SimSplit Vision API** documentation**. This is where 
we perform all things related to image processing, detection and 
classification. It is powered by Uvicorn, FastAPI, OpenCV, Mask-RCNN 
and Tesseract

## Async Callbacks
So the idea is that when a request is received by the API, an instant 
response is returned with success or failure. To indicate if the request 
went through. Then, after processing, a request is sent to the webhook with 
the success or failure of the operation with the associated data (if 
applicable.)

All webhooks will return the following format:

- data[identifiers] (always) ~ Contains UUIDs

- data[results] (sometimes) ~ Contains all kinds of data

```json
{
  "data": {
    "identifiers": {},
    "results": {}
  }
}
```

## Load Balancing Policy - Chaos Relays
We have a novel implementation of a pseudo-random distance-aware load 
balancing policy that is tightly integrated with a MongoDB instance.

We use actions that we term chaos relays, which are globally aware of 
distance between the source server and the relay server.

The reason this is implemented is due to the high CPU consumption time 
of AI-related processes and as such, we need a load balancer, that 
does not just route traffic equally, but one that sends traffic to the 
nearest available server. 

"""
