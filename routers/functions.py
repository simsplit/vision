#  ---------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  ---------------------------------------------------------------------

# Import OS
import os
import glob

# FastAPI library
from fastapi import APIRouter
from fastapi.responses import ORJSONResponse
from fastapi.logger import logger

# Google API exception
from google.api_core.exceptions import NotFound

# API validation models
from api_models.responses import responses, StandardResponse
from api_models.requests import (
    BlurDetectionRequest,
    ImageStitchingRequest,
    BinarizationRequest,
)

from processors.binarization_processor import BinarizationProcessor
from processors.stitching_processor import ImageStitchingProcessor
from processors.uploader_processor import GCSBackgroundWorker
from processors.gcs_master import GCSMaster

# Algorithms
from src.blur_detection import BlurDetector

from tempfile import TemporaryDirectory

router = APIRouter()


# Functions - Blur Detection
@router.post(
    "/blur-detection",
    responses=responses,
    response_model=StandardResponse,
    response_class=ORJSONResponse,
)
async def blur_detection(item: BlurDetectionRequest):
    """
    **Blur Detector** is an algorithm that classifies images as blurry
    or not by thresholding the variance of the Laplacian of the image.

    Takes an image path in the format: `bucket_name/path/to/file.tar.xz`
    """

    # Create a temporary directory
    temp_dir = TemporaryDirectory(dir="./")

    # Download image
    local_file = f"{temp_dir.name}/{item.filename}"
    # cloud_blob = f"users/{item.user_uuid}"
    # cloud_blob += f"/receipts/{item.receipt_uuid}/{item.filename}"
    cloud_blob = f"receipts/{item.receipt_uuid}/{item.filename}"
    # try:
    # Download the file from GCS
    GCSMaster(
        bucket_name=item.bucket_name,
        cloud_blob=cloud_blob,
        local_file=local_file,
        direction="down",
    )
    # except NotFound as e:
    #     # Compile response body
    #     response = dict(
    #         status=404,
    #         msg="Failure",
    #         body=dict(
    #             identifiers={
    #                 "user_uuid": item.user_uuid,
    #                 "receipt_uuid": item.receipt_uuid
    #             },
    #         ),
    #     )
    #     return response

    # Run the Blur Detector
    bd = BlurDetector(filename=local_file)

    # Delete files
    temp_dir.cleanup()

    # Compile response body
    response = dict(
        status=200,
        msg="Success",
        body=dict(
            identifiers={
                "receipt_uuid": item.receipt_uuid,
            },
            is_blurry=bool(bd.is_blurry),
        ),
    )

    return response


# Functions - Image Stitching
@router.post(
    "/image-stitching",
    response_model=StandardResponse,
    responses=responses,
    response_class=ORJSONResponse,
)
async def image_stitching(item: ImageStitchingRequest):
    """
    This algorithm essentially takes a list of images, sorted
    top-to-bottom, and attempts to stitch them together. Works
    flawlessly.
    """
    ImageStitchingProcessor(item=item).run()

    response = dict(
        status=200, msg="Received.", bdoy=dict(status="processing.")
    )

    return response


# Functions - Binarization
@router.post(
    "/binarization",
    response_model=StandardResponse,
    responses=responses,
    response_class=ORJSONResponse,
)
async def binarization(item: BinarizationRequest):
    """
    A binarization method that is better than OTSU's Thresholding.
    Resulting (by accidental discovery) in perfect correction of shade.
    OTSU would instead render any shaded areas in the receipt solid black.

    It also accepts certain methods:

    - **su** for SU binarization
    - **otsu** for OTSU thresholding

    """
    BinarizationProcessor(item=item).run()

    response = dict(
        status=200, msg="Received.", bdoy=dict(status="processing.")
    )

    return response
