#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

# FastAPI library
from fastapi import APIRouter, Depends
from fastapi.responses import ORJSONResponse

# API validation models
from api_models.responses import HealthResponse


router = APIRouter()


# Health checks
@router.get(
    "/",
    status_code=200,
    response_model=HealthResponse,
    response_class=ORJSONResponse,
)
async def root():
    """
    Simple health check at root.
    """
    return {"message": "success"}


# Liveness & Readiness Health Check
@router.get(
    "/healthz",
    status_code=200,
    response_model=HealthResponse,
    response_class=ORJSONResponse,
)
async def healthz():
    """
    Dedicated health check at /healthz to allow Kubernetes probes (on GCP)
    to test:

    - Liveness (through `livenessProbe`)
    - Readiness (through `readinessProbe`)

    These are usually specified in the `deployment.yaml` spec as follows:
    ```yaml
    spec:
      containers:
        # Health Checks
        livenessProbe:
          httpGet:
            path: "/healthz"
            port: 8000
          initialDelaySeconds: 10
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: "/healthz"
            port: 8000
          initialDelaySeconds: 15
          periodSeconds: 15
    ```

    """
    return {}
