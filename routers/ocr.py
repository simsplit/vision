#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

# FastAPI library
from fastapi import APIRouter
from fastapi.responses import ORJSONResponse
from fastapi.logger import logger

# API validation models
from api_models.requests import ReceiptOCRRequest
from api_models.responses import StandardResponse, responses

# GCS Upload/Download
from processors.ocr_processor import OCRProcessor

# LineSegmenter and OCR
from uuid import uuid4

from uuid import uuid4
import shutil
import os

router = APIRouter()


# OCR - receipt
@router.post(
    "/receipt",
    response_model=StandardResponse,
    status_code=200,
    responses=responses,
    response_class=ORJSONResponse,
    summary="Receipt OCR",
)
async def receipt_ocr(item: ReceiptOCRRequest):
    """
    This is where we perform actual OCR on the pre-processed receipt.

    We first run the LineSegmenter then the OCR.

    LineSegmenter is an algorithm, that when given a relatively
    low-entropy image, will create contours and segment the image into
    its constituent text boxes. It also maps the text boxes by row
    (and column if necessary)
    """
    # Expects a dictionary
    OCRProcessor(item=item).run()

    response = dict(
        status=200, msg="Received.", bdoy=dict(status="processing.")
    )

    return response
