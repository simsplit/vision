#  ---------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  ---------------------------------------------------------------------

# FastAPI library
from fastapi import APIRouter
from fastapi.responses import ORJSONResponse
from fastapi.logger import logger

# API validation models
from api_models.responses import StandardResponse, responses
from api_models.requests import ReceiptDetectronRequest
from processors.detectron_processor import DetectronProcessor

import glob
import os

router = APIRouter()


# Detectron - Receipt
@router.post(
    "/receipt",
    status_code=200,
    responses=responses,
    response_model=StandardResponse,
    response_class=ORJSONResponse,
)
async def receipt_detectron(item: ReceiptDetectronRequest):
    """
    We can take an image, and through a trained model, we detect the edges
    and a mask to a great degree of accuracy. This results in a finely-cropped
    image that we can use for other parts of the pre-processing pipeline.

    Args:
        :class:`ReceiptDetectronRequest`

    Returns:
        :class:`StandardResponse`
    """
    DetectronProcessor(item=item).run()

    response = dict(
        status=200, msg="Received.", bdoy=dict(status="processing.")
    )

    return response
