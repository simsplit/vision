# OS Container
FROM ubuntu:18.04

# Set working directory
WORKDIR /usr/src

# Update sources & install curl
RUN apt-get update && \
    apt-get install curl -y

# Add Tesseract Repository
RUN apt-get install gcc -y && \
    apt-get install g++ -y && \
    # apt-get install apt-utils -y && \
    apt-get install software-properties-common -y && \
    add-apt-repository ppa:alex-p/tesseract-ocr -y

# Install arabic and english tesseract
RUN apt-get update && \
    apt-get install tesseract-ocr-ara -y && \
    apt-get install tesseract-ocr-eng -y && \
    apt-get install libtesseract-dev -y

# Install python 3.7
RUN add-apt-repository ppa:deadsnakes/ppa -y && \
    # uvloop dependence (won't work unless -dev is installed)
    apt-get install python3.7-dev -y && \
    apt-get install python3-pip -y

# Install Git and clone Mask_RCNN repo
RUN apt-get install git -y && \
    git clone https://github.com/matterport/Mask_RCNN.git

# Copy the fixed dependency file
COPY _requirements.txt ./

# Replace requirements file with tested dependencies
RUN cp _requirements.txt ./Mask_RCNN/requirements.txt

# Install Mask_RCNN requirements
RUN python3.7 -m pip install -r ./Mask_RCNN/requirements.txt && \
    python3.7 -m pip install numpy==1.16.4

# Navigate into the direcotry and install Mask_RCNN
WORKDIR /usr/src/Mask_RCNN
RUN python3.7 -m pip install --upgrade setuptools && \
    python3.7 setup.py install

# Get libsm6 to prevent libSM.so.6 ImportError
RUN apt-get install -y libsm6 libxext6 libxrender-dev

# Navigate back to the project directory
WORKDIR /usr/src

# Install requirements
COPY requirements.txt ./
RUN python3.7 -m pip install -r requirements.txt