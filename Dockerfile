# Base image
FROM simsplit/base-detectron:latest

# Navigate back to the project directory
WORKDIR /usr/src

# Create models directory
RUN mkdir -p models

# Copy directory contents
COPY . .

# Expose port
EXPOSE 8000

# Run uvicorn and bind
CMD /bin/sh -c ./run.sh