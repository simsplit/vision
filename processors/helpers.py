# -*- coding: utf-8 -*-
from fastapi.logger import logger

from .gcs_master import GCSMaster

# Import for async io
import asyncio
import socket
import requests
from requests.exceptions import ConnectionError

# For the abstract class
from abc import ABC, abstractmethod

import asyncio
import functools

WEBHOOK_ID = "vision"
ALLOW_ASSERTIONS = True


# Abstract Base Class for Data Processes
class DataProcessor(ABC):
    """DataProcessor is an abstract base class, contains a single \
    method run(), which is usually overridden as is custom to an \
    ``@abstractmethod``.

    The __init__ receives a dictionary, and dynamically generates \
    class attributes based on the keys and values.
    """

    def __init__(self, **kwargs):
        # Set dynamic attributes
        for k, v in kwargs.items():
            setattr(self, k, v)

        # Set static attributes
        self.status = False
        super(DataProcessor, self).__init__()

    @abstractmethod
    def run(self, **kwargs):
        pass


# Fire and Forget
def fire_and_forget(f):
    """A decorator to execute functions in an event loop with no
    with no expectation of future callback."""

    def wrapped(*args, **kwargs):
        asyncio.set_event_loop(asyncio.new_event_loop())
        return asyncio.get_event_loop().run_in_executor(
            None, f, *args, *kwargs
        )

    return wrapped


def register_status(state, processor):
    assert state in ["0", "1"]
    with open(f"status/{processor}.status", "w") as f:
        f.write(state)


def read_status(processor):
    with open(f"status/{processor}.status", "r") as f:
        results = f.read()
    return results


# Update webhooks
def update_webhooks(**kwargs):
    """A decorator to update webhooks given a MongoDBConnection object"""
    # Assign MongoDBConnection object to var 'mdb'
    mdb = kwargs["mdb"]
    collection = mdb["simsplit"]["webhooks"]
    hook = kwargs["hook"]
    hook_keys = hook.split("/")

    def webhooks_entry_wrapper(func):
        @functools.wraps(func)
        def webhooks_entry(*args, **kwargs):
            # Get webhook entry
            obj = dict(_type=str(WEBHOOK_ID))

            # Find the entry
            item = collection.find_one(obj)
            # Loop over list of dictionary keys and subset accordingly
            for i in range(len(hook_keys)):
                item = item[hook_keys[i]]

            # Ensure they are lists
            if isinstance(item, str):
                kwargs["urls"] = [item]
                if ALLOW_ASSERTIONS:
                    assert isinstance(kwargs["urls"], list)
            else:
                kwargs["urls"] = item
                if ALLOW_ASSERTIONS:
                    assert isinstance(kwargs["urls"], list)

            mdb.close()

            value = func(*args, **kwargs)
            return value

        return webhooks_entry

    return webhooks_entry_wrapper
