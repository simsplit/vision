# -*- coding: utf-8 -*-
"""relay.py

This module contains a single class :class:`PodAwareRelay` which enables
us to have automatically-scalable kubernetes deployments.

"""
from fastapi.logger import logger


class PodAwareRelay:
    """This class :class:`PodAwareRelay` is as the name suggests, a way
    to relay requests with pod awareness, according to availability
    status, and distance inferred through regional and zonal information

    """

    def __init__(self, request, settings, logging=False):
        self.__request = request
        self.logging = logging
        self.pod_metadata = settings.pod_metadata
        if self.logging:
            logger.info(f"[path] {self.path}")
            logger.info(f"[headers] {self.headers}")

        # Properties
        self.counter = 0
        self.regions = []
        self.zones = []

    @property
    def path(self):
        """

        Returns:

        """
        request = self.__request
        return request.__dict__["scope"]["path"]

    @property
    def headers(self):
        """
        We process the request headers and update them to include the
        following values.

            `X-Relay-Req-UUID`
                The UUID of the request. Generated on first request.

            `X-Relay-Pod-Name`
                The hostname of the current (origin) pod.

            `X-Relay-Region`
                The region of the originating pod.

            `X-Relay-Zone`
                The zone of the originating pod

            `X-Relay-Counter`
                The number of jumps the request has performed.

            `X-Relay-Distance`
                The total distance crossed over.

            `X-Relay-Regional-Distance`

            `X-Relay-Zonal-Distance`

            `X-Relay


        Returns:

        """
        request = self.__request
        meta = self.pod_metadata
        __headers = {
            k.decode("utf-8"): v.decode("utf-8")
            for k, v in request.__dict__["scope"]["headers"]
        }
        # __headers.update(
        #     {
        #         "X-Relay-POD-Name": meta.get('name'),
        #         "X-Relay-GCP-Region": meta.get('location').get('region'),
        #         "X-Relay-GCP-Zone": meta.get('location').get('zone')
        #     }
        # )

        return __headers

    @property
    def counter(self):
        return self.__counter

    @counter.setter
    def counter(self, v):
        self.__counter = v
        return self.__counter

    @property
    def regions(self):
        return self.__regions

    @regions.setter
    def regions(self, v):
        self.__regions = v
        return self.__regions

    @property
    def zones(self):
        return self.__zones

    @zones.setter
    def zones(self, v):
        self.__zones = v
        return self.__zones

    @property
    def x_relay_jump_number(self):
        """

        Returns:

        """
        pass

    @property
    def x_relay_jump_distance(self):
        """

        Returns:

        """
        pass
