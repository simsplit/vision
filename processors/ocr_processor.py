# -*- coding: utf-8 -*-
from .helpers import (
    DataProcessor,
    fire_and_forget,
    register_status,
    update_webhooks,
)
from .gcs_master import GCSMaster
from tempfile import TemporaryDirectory
from src.line_segmentation import LineSegmenter
from src.ocr import OCR
from helpers.settings import settings
from .uploader_processor import GCSBackgroundWorker
from fastapi.logger import logger

from uuid import uuid4

import requests
import shutil
import os

mdb_client = settings.create_mongo_client()


class OCRProcessor(DataProcessor):
    """Forked Uploads/Downloads

    """

    @fire_and_forget
    @update_webhooks(mdb=mdb_client, hook="receipt/ocr")
    def run(self, **kwargs):
        """

        Args:
            **kwargs ():

        Returns:

        """
        logger.info(">>> Running OCR pipeline...")

        # 1 is busy
        register_status("0", "ocr")

        item = self.item

        temp_dir = TemporaryDirectory(dir="./")

        file_dict = {}
        for k, v in item.file_dict.items():
            # Download image
            local_file = f"{temp_dir.name}/{v}"
            cloud_blob = f"receipts/{item.receipt_uuid}/{v}"

            GCSMaster(
                bucket_name=item.bucket_name,
                cloud_blob=cloud_blob,
                local_file=local_file,
                direction="down",
            )
            file_dict[k] = local_file

        logger.info("> Performing line segmentation...")
        # Run the Line Segmentation Algorithm on the binarized image and create
        # the segments from the cropped image.
        ls = LineSegmenter(temp_dir=temp_dir, file_dict=file_dict)

        logger.info("> Performing OCR...")
        # Run the OCR on the cropped image
        ocr = OCR(temp_dir=temp_dir, filename="detections.json")
        ocr.compute()

        # Fork uploads to background process
        files = []

        # Upload segments
        for segment in ls.segment_names:
            file_spec = {}
            # Upload the segment
            filename = segment
            cloud_blob = f"receipts/{item.receipt_uuid}/{filename}"

            local_file = f"{temp_dir.name}/{filename}"

            file_spec["bucket_name"] = item.bucket_name
            file_spec["cloud_blob"] = cloud_blob
            file_spec["local_file"] = local_file
            file_spec["direction"] = "up"
            files.append(file_spec)

        # Upload Boxes
        for filename in [
            "boxes.png",
            # 'boxes-paired.png',
            "detections.json",
        ]:
            file_spec = {}
            cloud_blob = f"receipts/{item.receipt_uuid}/{filename}"

            local_file = f"{temp_dir.name}/{filename}"

            file_spec["bucket_name"] = item.bucket_name
            file_spec["cloud_blob"] = cloud_blob
            file_spec["local_file"] = local_file
            file_spec["direction"] = "up"
            files.append(file_spec)

        # # Fork uploads to the background
        # GCSBackgroundWorker(temp_dir=temp_dir, files=files).run()

        # Compile response body
        response = dict(
            status=200,
            msg="Success",
            body=dict(
                identifiers={
                    "receipt_uuid": str(item.receipt_uuid),
                },
                data=ocr.results,
            ),
        )

        for url in kwargs["urls"]:
            res = requests.post(
                url=url,
                headers={"Content_type": "application/json"},
                json=response,
            )
            logger.info(f"> Webhook sent to {url}")
            if res.status_code != 200:
                logger.warning(f"> Webhook status: {res.status_code}")

        for f in files:
            # Uploading
            GCSMaster(
                bucket_name=f["bucket_name"],
                cloud_blob=f["cloud_blob"],
                local_file=f["local_file"],
                direction=f["direction"],
            )

        # Delete files
        temp_dir.cleanup()
        logger.info(f"> Cleaned up directory {temp_dir.name}")

        # 0 is ready
        register_status("1", "ocr")

        logger.info(">>> Processing complete.")
