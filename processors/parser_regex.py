# -*- coding: utf-8 -*-
"""parser_regex.py is a module containing a few variables, each of which
extracts certain types of text through REGEX pattern matching.

Examples:
    A simple way to use these is as follows:
        >>> import re
        >>> re.findall('x www.example.com/page1 *sc1')
        >>> ['www.example.com/page1']

"""
# Regex pattern for extracting prices
PRICE_REGEX: str = r"(?:[\£\$\€]{1}[,\d]+.?\d*)"

# Regex pattern for extracting digits
DIGIT_REGEX: str = r"(?:[,\d]+.?\d*)"


# Regex pattern for extracting percentages
PERCENTAGE_REGEX: str = r"\b(?<!\.)(?!0+(?:\.0+)?%)(?:\d|[1-9]\d|100)(?:(?<!" r"100)\.\d+)?%"

# Regex pattern for extracting urls
URL_REGEX = (
    r"((?<=[^a-zA-Z0-9])(?:https?\:\/\/|[a-zA-Z0-9]{1,}\.{1}|\b)(?:"
    r"\w{1,}\.{1}){1,5}(?:com|org|edu|gov|uk|net|ca|de|jp|fr|au|us|"
    r"ru|ch|it|nl|se|no|es|mil|iq|io|ac|ly|sm){1}(?:\/[a-zA-Z0-9]"
    r"{1,})*)"
)
