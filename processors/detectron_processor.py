# -*- coding: utf-8 -*-
from .helpers import (
    DataProcessor,
    fire_and_forget,
    register_status,
    update_webhooks,
)
from .gcs_master import GCSMaster
from tempfile import TemporaryDirectory

from src.receipt_detectron import ReceiptDetectron
from helpers.settings import settings
from .uploader_processor import GCSBackgroundWorker
from fastapi.logger import logger
from google import api_core

from uuid import uuid4

import requests
import shutil
import os

mdb_client = settings.create_mongo_client()


class DetectronProcessor(DataProcessor):
    """Forked Uploads/Downloads

    """

    @fire_and_forget
    @update_webhooks(mdb=mdb_client, hook="receipt/detectron")
    def run(self, **kwargs):
        """

        Args:
            **kwargs ():

        Returns:

        """
        logger.info(">>> Running detectron...")

        # 1 is busy
        register_status("0", "detectron")

        item = self.item

        # Create a temporary directory
        temp_dir = TemporaryDirectory(dir="./")

        # Download image
        local_file = f"{temp_dir.name}/{item.filename}"
        cloud_blob = f"receipts/{item.receipt_uuid}/{item.filename}"

        GCSMaster(
            bucket_name=item.bucket_name,
            cloud_blob=cloud_blob,
            local_file=local_file,
            direction="down",
        )

        # Run the detectron
        detectron = ReceiptDetectron(temp_dir=temp_dir, image_path=local_file)

        # Upload the masked image
        files = []
        # Upload the image
        for filename in [
            detectron.masked_image_name,
            detectron.cropped_image_name,
        ]:
            file_spec = {}
            cloud_blob = f"receipts/{item.receipt_uuid}/{filename}"

            local_file = f"{temp_dir.name}/{filename}"

            file_spec["bucket_name"] = item.bucket_name
            file_spec["local_file"] = local_file
            file_spec["cloud_blob"] = cloud_blob
            file_spec["direction"] = "up"
            files.append(file_spec)

            # Fork uploads to the background
            # GCSBackgroundWorker(temp_dir=temp_dir, files=files).run()

        for f in files:
            # Uploading
            GCSMaster(
                bucket_name=f["bucket_name"],
                cloud_blob=f["cloud_blob"],
                local_file=f["local_file"],
                direction=f["direction"],
            )

        # Construct Response
        response = dict(
            status=200,
            msg="Success",
            body=dict(
                identifiers={
                    "receipt_uuid": str(item.receipt_uuid),
                }
            ),
        )

        # Delete files
        temp_dir.cleanup()
        logger.info(f"> Cleaned up directory {temp_dir.name}")

        for url in kwargs["urls"]:
            res = requests.post(
                url=url,
                headers={"Content_type": "application/json"},
                json=response,
            )
            logger.info(f"> Webhook sent to {url}")
        # 0 is ready
        register_status("1", "detectron")

        logger.info(">>> Processing complete.")
