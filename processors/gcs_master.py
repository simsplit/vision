#  -----------------------------------------------------------------------------
#  SimSplit.co Copyright (c) 2020.
#
#  Author: Yanal Kashou
#  Email: technology@simsplit.co
#  Organization: SimSplit.co
#  Python Version: python:3.7
#  -----------------------------------------------------------------------------

from google.cloud import storage
from dataclasses import dataclass
from fastapi.logger import logger


# ------------------------------------------------------
# Bucket Dataclass
# ------------------------------------------------------
@dataclass
class BucketMeta:
    """BucketMeta (Bucket metadata)

    Args:
        bucket_name (:obj:`str`): Name of bucket
        cloud_blob (:obj:`str`): File path of the blob on GCS
        local_file (:obj:`str`): Local file

    """

    bucket_name: str
    cloud_blob: str
    local_file: str


# ------------------------------------------------------
# Google Cloud Storage Master
# ------------------------------------------------------
@dataclass
class GCSMaster(BucketMeta):
    """Downloads a file from a blob on GCS,
    or Uploads a file to a blob on GCS

    Examples:
        >>> bucket_name = "your-bucket-name"
        >>> cloud_blob = "remote/path/to/file.tar.xz"
        >>> local_file = "local/path/to/file.tar.xz"
        # To Upload
        >>> GCSMaster(bucket_name, cloud_blob, local_file, 'up')
        # To Download
        >>> GCSMaster(bucket_name, cloud_blob, local_file, 'down')

    """

    direction: str = "up"

    def __post_init__(self):

        storage_client = storage.Client()
        bucket = storage_client.bucket(self.bucket_name)
        blob = bucket.blob(self.cloud_blob)

        if self.direction == "up":
            blob.upload_from_filename(self.local_file)
            logger.info(f"> Uploaded file to {self.cloud_blob}")
        elif self.direction == "down":
            blob.download_to_filename(self.local_file)
            logger.info(f"> Downloaded blob to {self.local_file}.")


if __name__ == "__main__":
    data = {
        "bucket_name": "simsplit-model-zoo-staging",
        "cloud_blob": "sample_receipts/original/test.jpg",
        "local_file": "test.jpg",
        "direction": "up",
    }
    gcs = GCSMaster(**data)
