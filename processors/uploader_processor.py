# -*- coding: utf-8 -*-
from .helpers import DataProcessor, fire_and_forget
from .gcs_master import GCSMaster
from helpers.settings import settings
from fastapi.logger import logger


class GCSBackgroundWorker(DataProcessor):
    """Forked Uploads/Downloads

    """

    @fire_and_forget
    def run(self, **kwargs):
        """

        Args:
            **kwargs ():

        Returns:

        """
        for f in self.files:
            # Download the file from GCS
            GCSMaster(
                bucket_name=f["bucket_name"],
                cloud_blob=f["cloud_blob"],
                local_file=f["local_file"],
                direction=f["direction"],
            )

        # Delete files
        self.temp_dir.cleanup()
        logger.info(f"> Cleaned up directory {self.temp_dir.name}")

        # cl = MongoClient(MONGO_URI)['pod_registrar']['vision-api']
        # cl.delete_one(dict(name=pod_metadata['name']))
        # logger.info(settings.pod_metadata)
