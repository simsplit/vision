#!/usr/bin/env bash
# Deregister a pre-existing pod from mongo by calling a hook to
# relay-manager

# Pod Name
pod_name=`hostname`

# Construct header with TOKEN
header=`printf 'x-api-token: %s' "$JWT_TOKEN"`

# Call request
curl --location --request POST 'http://0.0.0.0:7900/api/v1/relay/pod/deregister' \
    --header "$header" \
    --header 'Content-Type: application/json' \
	--data-raw '{
	    "name": "'"$pod_name"'"
	}'
