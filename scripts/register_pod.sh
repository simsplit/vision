#!/usr/bin/env bash
# Register a pod into mongo by calling a hook to relay-manager

# Pod Name
pod_name=`hostname`

# Pod IP
pod_addr=`hostname -i`

# Get the pod region/zone data
url="http://metadata.google.internal/computeMetadata/v1/instance/zone"
res=$(curl -H "Metadata-Flavor: Google" -sX GET ${url})

# Keep only the region and zone
res=${res##*/}

# Split to get the region and zone separately (europe-west4 AND a)
region=${res%-*}
zone=${res##*-}

# Construct header with TOKEN
header=`printf 'x-api-token: %s' "$JWT_TOKEN"`

# Call request
curl --location --request POST 'http://0.0.0.0:7900/api/v1/relay/pod/register' \
--header "$header" \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "'"$pod_name"'",
    "addr": "'"$pod_addr"'",
    "region": "'"$region"'",
    "zone": "'"$zone"'",
    "availability": "1"
}'