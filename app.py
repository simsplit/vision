# -*- coding: utf-8 -*-
"""app.py

This API implemented the following:
    1. Automated documentation
    2. Pod registration on startup
    3. Pod deregistration on shutdown
    4. Pod-aware relay middleware
    5. Simple API authentication via token
    6. Multiple routers such as health checks, generic functions,
    detectron and ocr.

"""
# General libs
import os
import orjson
import requests

# FastAPI
from fastapi import FastAPI, Depends, Header, Request, HTTPException
from fastapi.logger import logger

# Vision API
from routers import health, functions, ocr, detectron
from api_models.responses import responses
from helpers.settings import settings
from helpers.markdown import title, version, description
from bson import ObjectId
from processors.pod_aware_relay import PodAwareRelay
from processors.helpers import read_status

# App Settings
from helpers.settings import settings

# Initialize app
app = FastAPI(
    title=title,
    version=version,
    description=description,
    openapi_url=f"/{settings.api_version}/openapi.json",
    docs_url=f"/{settings.api_version}/docs/swagger",
    redoc_url=f"/{settings.api_version}/docs/redoc",
)

# Copy pod registrar collection client to global scope
global pod_registrar

mdb_client = settings.create_mongo_client()
pod_registrar = mdb_client["simsplit"]["pod_registrar"]

# pod_registrar = settings.pod_registrar_cl

# Generate a unique ID for the document
pod_id = ObjectId()

RELAY = bool(int(os.getenv("POD_AWARE_RELAY")))


# Create a relay middleware
@app.middleware("http")
async def relay_requests(request: Request, call_next):
    """relay_requests API middleware

    Process:
        * A request comes in, triggering the PodAwareRelay.

        * It initializes the path and headers, and other relevant
        properties.

        * Path exemptions

        * If the path is exempt from relaying then the request is called
        and a response expected.

        * If the path is not exempt (requires a relay), then status is
        checked.

        * If the status = 1, which means the server is available, then
        call the request and generate a response.

        * If the status = 0, which means the server is not available,
        then get the current list of servers, choose by distance,
        and then send a webhook to redirect the request to the next
        server with the updated headers.

    Args:
        request ():
        call_next ():

    Returns:

    """
    # A flag to turn the relay on and off
    if RELAY:
        # Initialize the PodAwareRelay
        relay = PodAwareRelay(
            request=request, settings=settings, logging=False
        )

        # Get url path
        # Get headers as dict
        # Get pod name
        # Get exempted paths
        path = relay.path
        headers = relay.headers
        pod_name = settings.pod_metadata["name"]
        path_exemptions = settings.path_exemptions

        # CASE NO.1
        # If path is exempt from relay, simply call the request
        # such as /, /healthz and /blur-detection
        if path in path_exemptions:
            # Call the request naturally
            response = await call_next(request)

        # CASE NO.2
        # Otherwise, if path is not exempt from relaying (i.e. long request)
        # such as /detectron and /ocr
        else:
            # Read the status files and determine if an event loop is
            # running or not.
            available = all(
                [
                    bool(int(read_status("ocr"))),
                    bool(int(read_status("detectron"))),
                    bool(int(read_status("stitcher"))),
                    bool(int(read_status("binarization"))),
                ]
            )
            if available:
                # Call the request naturally
                response = await call_next(request)
            if not available:
                # Before calling the request, we need to get the current
                # availability status of the current pod, then call the
                # request and await the response to be fulfilled.
                results = pod_registrar.find_one(dict(name=pod_name))
                # TODO:
                #       If the pod is available:
                #           1. mark it as as unavailable
                #           2. process the request.
                #           3. mark the pod as available again.
                #       If the pod is not available:
                #           1. relay the request.

                # If results were returned (pod is registered) and pod
                # is available (1)
                if results and (results["availability"] == 1):
                    logger.warning(
                        "[1a] Results returned and pod is available"
                    )
                    # Copy the results
                    new_dictionary = results.copy()

                    # Set as unavailable and insert
                    new_dictionary["availability"] = 0
                    pod_registrar.update_one(
                        dict(name=pod_name),
                        {"$set": new_dictionary},
                        upsert=True,
                    )
                    logger.warning("Calling request")
                    # The request is now called
                    response = await call_next(request)

                    # Set as available and insert
                    new_dictionary["availability"] = 1
                    pod_registrar.update_one(
                        dict(name=pod_name),
                        {"$set": new_dictionary},
                        upsert=True,
                    )
                # If results were returned (pod is registered) and pod
                # is not available (0)
                elif results and (results["availability"] == 0):
                    logger.warning(
                        "[1b] Results returned and pod is NOT available"
                    )
                    # GET NEAREST AVALABLE POD
                    hook_url = "http://0.0.0.0:7900/api/v1/relay/pods/selector"
                    hook_data = dict(
                        name=results["name"],
                        region=results["region"],
                        zone=results["zone"],
                    )
                    hook_headers = {
                        "Content-type": "application/json",
                        "x-api-token": os.getenv("JWT_TOKEN"),
                    }

                    response = requests.get(
                        url=hook_url, headers=hook_headers, json=hook_data
                    )

                    res_data = orjson.loads(response.text)["body"]["pods"][0]

                    # Path is now fixed with port and without double '/'
                    relay_url = f'http://{res_data["addr"]}:8000{path}'
                    logger.warning(f"[2] Constructed relay url: {relay_url}")
                    relay_data = request.items
                    logger.warning(
                        f"[3] Request items look like: {request.items}"
                    )

                    relay_headers = {
                        "Content-type": "application/json",
                        "x-api-token": os.getenv("JWT_TOKEN"),
                    }

                    # IMPORTANT RELAY
                    response = requests.post(
                        url=relay_url, headers=relay_headers, json=relay_data
                    )
                    logger.warning(
                        f"[4] Response status of relay is {response}"
                    )

                # If pod is not registered, proceed as normal
                else:
                    logger.warning("[1c] No results were returned.")
                    # The request is now called
                    response = await call_next(request)

        # Return the main response object (independant of the relay)
        return response

    # If the relay is turned off, then just call the request normally.
    else:
        response = await call_next(request)
        return response


# Create an x_api_token checker
async def get_token_header(x_api_token: str = Header(...)):
    """

    Args:
        x_api_token (:obj:`str`):  A token to match a pre-generated one.

    Raises:
          HTTPException if failed equality.
    """
    if x_api_token != os.getenv("JWT_TOKEN"):
        raise HTTPException(
            status_code=400, detail="X-API-Token header invalid"
        )


# Include the health router
app.include_router(health.router, tags=["Health Checks"], responses=responses)

# Include the functions router
app.include_router(
    functions.router,
    prefix=f"/{settings.api_version}/functions",
    tags=["Image Processing Functions"],
    dependencies=[Depends(get_token_header)],
)

# Include the detectron router
app.include_router(
    detectron.router,
    prefix=f"/{settings.api_version}/detectron",
    tags=["Detectrons (Mask-RCNN, YOLOv3)"],
    dependencies=[Depends(get_token_header)],
)

# Include the ocr router
app.include_router(
    ocr.router,
    prefix=f"/{settings.api_version}/ocr",
    tags=["Optical Character Recognition (OCR)"],
    dependencies=[Depends(get_token_header)],
)


# After the request is made, we update the headers containing
# tracking metadata.
# X-Relay-Jump for instance represents the number of jumps so far
# across the PodAwareRelay. (This limited to 10 jumps by default and
# can be modified in the deployment spec.
# if 'X-Relay-Jump' in headers.keys():
#     n_jump = int(headers['X-Relay-Jump'])
#
#     if n_jump <= settings.max_jumps:
#         # TODO break the loop
#         pass
#     else:
#         n_jump += 1
#         response.headers["X-Relay-Jump"] = str(n_jump)
#
# else:
#     response.headers["X-Relay-Jump"] = str(1)
