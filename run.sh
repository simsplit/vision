#!/bin/bash

# Download models, etc..
python3.7 preload.py &&

# Run the API with Uvicorn
uvicorn --reload --host 0.0.0.0 --port 8000 app:app

